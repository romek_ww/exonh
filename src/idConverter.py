import SOAPpy
from SOAPpy import WSDL
from customLogger import Logger

class IdConverter:

	l = Logger()

	def convert(self, pdbIdsIn):

		uniProtHeader='UniProt Entry Name'
		transcriptHeader='Ensembl Transcript ID'
	
		biodbnet = WSDL.Proxy('http://biodbnet.abcc.ncifcrf.gov/webServices/bioDBnet.wsdl')

		# tlumaczenie PDB ID -> UniProt Entry Name
		self.l.log('tlumaczenie PDB ID -> UniProt Entry Name')

		pdbValuesInput = ','.join(map(str, pdbIdsIn))
		inputPdb = 'PDB ID'
		outputUniProtEntry = 'UniProt Entry Name'
		#taxonId = '9606' #human


		#db2dbParams = SOAPpy.structType({'input': inputPdb, 'inputValues': pdbValuesInput, 'outputs': outputUniProtEntry, 'taxonId': taxonId})
		db2dbParams = SOAPpy.structType({'input': inputPdb, 'inputValues': pdbValuesInput, 'outputs': outputUniProtEntry})
		uniProtEntriesResult = biodbnet.db2db(db2dbParams)
		uniProtEntrieLines = uniProtEntriesResult.split('\n')

		pdbIds = []
		chainsNos = []
		uniProtEntries = []

		first = True
		for line in uniProtEntrieLines:
			if first==False and len(line)>0:
				splitLine = line.split('\t')
				uniProtNames = splitLine[1].split(';')
				chainCounter = 0
				for uniProtName in uniProtNames:
					if uniProtName!=uniProtHeader:
						pdbIds.append(splitLine[0]);
						uniProtEntries.append(uniProtName)
						# !TODO! zalozenie ze dostane lancuchy w kolejnsci zgodnej z pdb - zrobic ladniej !
						chainsNos.append(chainCounter)
						chainCounter = chainCounter+1
			else:
				first=False

		# tlumaczenie UniProt Entry Name -> Ensembl Transcript ID
		self.l.log('tlumaczenie UniProt Entry Name -> Ensembl Transcript ID')

		uniprotInput = ','.join(map(str, uniProtEntries))
		inputUniprot = 'UniProt Entry Name'
		outputTranscripts = 'Ensembl Transcript ID'

		#db2dbParams = SOAPpy.structType({'input': inputUniprot, 'inputValues': uniprotInput, 'outputs': outputTranscripts, 'taxonId': taxonId})
		db2dbParams = SOAPpy.structType({'input': inputUniprot, 'inputValues': uniprotInput, 'outputs': outputTranscripts})
		transcriptsResult = biodbnet.db2db(db2dbParams)
		transcriptsLines = transcriptsResult.split('\n')

		transcriptIds = []

		first = True
		for line in transcriptsLines:
			if first==False and len(line)>0:
				if line.split('\t')[1]!=transcriptHeader:
					transcriptIds.append(line.split('\t')[1])
			else:
				first=False

		# jesli dla bialka nie ma informacji o transkryptach pomijamy je w dalszej analizie zapisujac informacje o tym w logu 	
		for c in reversed(range(0, len(transcriptIds))):
			if '-' == transcriptIds[c]:
				self.l.error('no transcript found, deleting chainNo ' + str(chainsNos[c]) + ' for: ' + pdbIds[c])
				del transcriptIds[c]
				del pdbIds[c]
				del uniProtEntries[c]
				del chainsNos[c]
		
		
		return [pdbIds, uniProtEntries, transcriptIds, chainsNos]