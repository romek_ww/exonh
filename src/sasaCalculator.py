from Bio.PDB.PDBParser import PDBParser
from Bio.PDB.DSSP import DSSP
from customLogger import Logger
import csv

# secondary structure codes:
# H Alpha helix (4-12)
# B Isolated beta-bridge residue
# E Strand
# G 3-10 helix
# I pi helix
# T Turn
# S Bend
# - None

class SasaCalculator:

	def __init__(self, dsspExecutable):
		self.dsspExecutable = dsspExecutable

	def perform(self, pdbId):
	
		l = Logger()

		pdbFile = './pdb/' + pdbId + '.pdb'

		parser = PDBParser()
		structure = parser.get_structure(pdbFile,pdbFile)

		model = structure[0]

		dssp = DSSP(model, pdbFile, self.dsspExecutable)

		outData = []
		
		for residueData in list(dssp):
			residueObject, secStructure, solventAccesibility, relAcc, phi, psi = residueData
			resName = residueObject.get_resname()
			resId = residueObject.get_id()[1]
			chain = residueObject.get_parent()
			chainId = chain.get_id()
			outData.append((resId, resName, chainId, secStructure, solventAccesibility, relAcc))
		
		header = [('PDB_NO', 'resName', 'chain', 'secStructure', 'solventAccesibility', 'relAcc')]
		result = header + outData
		
		fileName = './sasa/' + pdbId + '.csv'
		
		with open(fileName, 'wb') as f:
			writer = csv.writer(f)
			writer.writerows(result)
			
		return outData		