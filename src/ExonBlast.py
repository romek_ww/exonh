from Bio.Blast.Applications import NcbiblastnCommandline
from Bio.Blast import NCBIXML
from StringIO import StringIO
from aligment import Aligment
from customLogger import Logger

class ExonBlast:

	l = Logger()

	def __init__(self, threadsCnt, eval):
		self.threadsCnt = threadsCnt
		self.eval = eval
	
	def blast(self, exon):
		
		fileName = exon.getSequenceFile()
		
		self.l.logF('-----------BLAST----------- for ' + fileName)
		
		out= NcbiblastnCommandline(query=fileName, db = "exons/exonsDB", outfmt=5, num_threads=self.threadsCnt, evalue=self.eval)()[0]

		if not out:
			return

		blast_result_record = NCBIXML.read(StringIO(out))
		
		try:
			for alignment in blast_result_record.alignments:
				title = alignment.title.encode('utf-8')
				aligmentExonID = title.split(' ')[1]
				if	aligmentExonID!=exon.getExonId():
					self.l.logF('**Alignment**' + aligmentExonID + ' len: ' + str(alignment.length))
					score = 0
					EValSum = 0
					
					for hsp in alignment.hsps:
						score = score + hsp.score
						EValSum = EValSum + hsp.expect
						self.l.logF('score: ' + str(hsp.score) + ' gaps: ' + str(hsp.gaps) + ' expect: ' + str(hsp.expect) + ' identities: ' + str(hsp.identities) + ' num_alignments: ' + str(hsp.num_alignments) + ' positives: ' + str(hsp.positives))
					
					avgEVal = EValSum/len(alignment.hsps)
					aligment = Aligment(aligmentExonID, alignment.length, score, avgEVal)
					
					exon.addSimilarExon(aligment)
				
		except IndexError:
			return False
		
		#hsp = alignment.hsps[0]
		#return Alignment(query=hsp.query, match=hsp.match, subject=hsp.sbjct)