from os import listdir
from os.path import isfile, join
from subprocess import call
from customLogger import Logger

class BlastDBMaker:

	l = Logger()
	
	def make(self):
		
		# stworzenie z wszystkich plikow fasta z sekwencjami exonow jednego - wymagane
		# do wysylania programu makeblastdb 
		
		path = 'exons'
		allExonsFile = join(path, 'allExonSequences.txt')
		
		filenames = [ f for f in listdir(path) if isfile(join(path,f))]
		with open(allExonsFile, 'w') as outfile:
			for fname in filenames:
				if fname[-5:].lower()=='fasta':  # tylko pliki z rozszezeniem fasta
					with open(join(path, fname)) as infile:
						outfile.write(infile.read())
						outfile.write('\n')
					
		# stworzenie bazy danych dla blasta

		exeName = 'makeblastdb.exe'
		
		cmdArgs = [exeName,
					'-dbtype', 'nucl',
					'-in', allExonsFile,
					'-title', 'exonsDB',
					'-out', join(path, 'exonsDB')]
					
		self.l.log('tworzenie bazy 	sekwencji exonow...')		
		
		call(cmdArgs)
		
		self.l.log('done')