from customLogger import Logger

class AssignExonDomains:

	l = Logger()

	def __init__(self, pfamDomains, assigments, lenThreshold):
		self.domains = pfamDomains
		self.assigments=assigments
		self.lenThreshold = lenThreshold
		
	def assign(self, exons):
		
		exonsOut = []
		
		for exon in exons:
		
			pdb = exon.getPdbId() + '_' + exon.getChain()
		
			if pdb in self.assigments:
				trnStart = exon.getStartAA()
				trnEnd = exon.getEndAA()
			
				assign = self.assigments[pdb]
				pdbAssign = assign[0]
				trnAssign = assign[1]
				
				#korekcja wspolrzednej konca transkryptu jesli wychodzi poza dlugosc bialka
				trnLen = len(trnAssign.replace('-', ''))
				if trnEnd>trnLen:
					trnEnd=trnLen
					exon.setEndAA(trnLen)
				
				pdbCount = 0;
				trnCount = 0;
				
				pdbMax = len(pdbAssign.replace('-', ''))
				
				for i in range(0, len(pdbAssign)):
					
					if pdbAssign[i]!='-':
						pdbCount = pdbCount+1
					
					if trnAssign[i]!='-':
						trnCount = trnCount+1
					else:	
						continue
						
					if	trnCount==trnStart:
						exon.setStartPdb(pdbCount)
						if pdbCount==pdbMax:
							#ostatni aminokwas z pdb chcemy przypisac jako numer poczatku exonu - czyli exon nie zmiescil sie w tym co jest w pdb - zerujemy
							exon.setStartPdb(-1)
							exon.setEndPdb(-1)
							break
						
						
					if	trnCount==trnEnd:
						exon.setEndPdb(pdbCount)
						if exon.getStartPdb()==0 and exon.getEndPdb()!=0:
							exon.setStartPdb(1)
						break

			else:
				self.l.log('BRAK DOPASOWANIA DLA: ' + pdb)
				
			# odzucamy zbyt krotkie exony

			if exon.getEndPdb()-exon.getStartPdb()<self.lenThreshold:
				self.l.log('exon too short: ' + exon.getExonId() + ', startPdb: ' + str(exon.getStartPdb()) + ' endPdb: ' + str(exon.getEndPdb()))
			else:
				exonsOut.append(exon)
			
			# dopasowanie do exonu domen pfam
			
			if len(self.domains)!=0:
				
				for domain in self.domains:
					
					if domain.getChain()==exon.getChain():
						exonInterval = [exon.getStartPdb(), exon.getEndPdb()]
						domainInterval = [domain.getPdbResNumStart(), domain.getPdbResNumEnd()]
						
						if self.isOverlap(exonInterval, domainInterval):
							exon.addDomain(domain)
				
			else:
				self.l.log('brak domen:' + exon.getPdbId())
				
		return exonsOut		
				
	def isOverlap(self, a, b):
		try:
			overSize = min(int(a[1]), int(b[1])) - max(int(a[0]), int(b[0]))
		except ValueError:
			return False
			
		return overSize>=0