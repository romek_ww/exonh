from customLogger import Logger

class Aligment:

	l = Logger()

	def __init__(self, exonId, aligmentLength, totalScore, averageE):
		self.exonId = exonId
		self.aligmentLength = aligmentLength
		self.averageE = averageE
		self.totalScore = totalScore
		
	# zwraca id znalezionego exonu
	def getExonId(self):
		return self.exonId
		
	# zwraca dlugosc calego 'query'
	def getAligmentLength(self):
		return self.aligmentLength	
		
	# suma score dla wszystkich hsp (score = positive - gaps)
	def getTotalScore(self):
		return self.totalScore

	# zwraca srednie Evalue
	def getAverageE(self):
		return self.averageE
		
	#loguje informacje o obiekcie	
	def printInfo(self):
		self.l.log('similar EXON ID: ' + self.exonId + ', aligmentLength: ' + str(self.aligmentLength) + ', averageE: ' + str(self.averageE) + ', totalScore: ' + str(self.totalScore))