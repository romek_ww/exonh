from customLogger import Logger

class SasaToExons:

	def assign(self, exons, sasa, chainId):
	
		l = Logger()
	
		#odfiltrowanie lancucha z ktorego sa exony
		sasaChain = [x for x in sasa if x[2] == chainId]
		#sortowanie po pdbNo
		sasaChain = sorted(sasaChain, key=lambda aminoAcid: aminoAcid[0])	
		
		for exon in exons:
			exonStart = exon.getStartPdb()
			exonEnd = exon.getEndPdb()
			
			avgSolventRelAcc = 'NA'
			secStrShares = {}	
			
			solventRelAccSum = 0.0
			aminoAcidsInExon = 0
			
			counter = 0
			for aminoAcid in sasaChain:
			
				if aminoAcid[5]=='NA':
					continue;
			
				counter = counter + 1
				if counter<=exonEnd and counter>=exonStart:
					aminoAcidsInExon=aminoAcidsInExon+1
					solventRelAccSum = solventRelAccSum + float(aminoAcid[5])
					
					secStr = aminoAcid[3]
					
					if secStr in secStrShares:
						secStrShares[secStr]=secStrShares[secStr]+1
					else:
						secStrShares[secStr]=1
			
			if aminoAcidsInExon==0:
				l.log('no amino acids for exon: ' + exon.getExonId())
				continue
			
			avgSolventRelAcc = solventRelAccSum/aminoAcidsInExon

			exon.setSecStrComposition(dict(secStrShares))

			for key, value in secStrShares.iteritems():
				secStrShares[key]=float(value)/aminoAcidsInExon
			
			exon.setAvgSolventRelAcc(avgSolventRelAcc)
			exon.setSecStrShares(secStrShares)