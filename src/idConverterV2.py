import SOAPpy
from SOAPpy import WSDL
from customLogger import Logger
import urllib,urllib2

class IdConverter:

	l = Logger()

	def convert(self, pdbIdsIn):
	
		pdbIds = []
		chainsNos = []
		uniProtEntries = []
		transcriptIds = []
		
		# tlumaczenie PDB ID -> UniProt Entry Name
		self.l.log('tlumaczenie PDB ID -> UniProt Entry Name')

		pdbValuesInput = ' '.join(map(str, pdbIdsIn))

		url = 'http://www.uniprot.org/mapping/'

		params = {
		'from':'PDB_ID',
		'to':'ACC',
		'format':'tab',
		'query':pdbValuesInput
		}

		data = urllib.urlencode(params)
		request = urllib2.Request(url, data)
		request.add_header('User-Agent', 'Python')
		response = urllib2.urlopen(request)
		page = response.read()
		lines = page.split('\n')

		
		first = True
		chainCounter=0
		lastPdbId=''
		
		for line in lines:
			if first==False and len(line)>0:
				pdbId, uniProtAcc = line.split('\t')
				pdbIds.append(pdbId)
				uniProtEntries.append(uniProtAcc)
				
				# !TODO! zalozenie ze dostane lancuchy w kolejnsci zgodnej z pdb - zrobic ladniej !
				if lastPdbId==pdbId:
					chainCounter = chainCounter+1
				else:
					chainCounter=0
					
				lastPdbId=pdbId
				chainsNos.append(chainCounter)
				
				self.l.log('uniProt mapping ' + pdbId + ':' + uniProtAcc + ' ' + str(chainCounter) )
			else:
				first=False	

				

		# tlumaczenie UniProt Entry Name -> Ensembl Transcript ID
		self.l.log('tlumaczenie UniProt Entry Name -> Ensembl Transcript ID')

		uniprotToTranscript={}
		
		uniqUniprotIds = set(uniProtEntries)
		uniprotValuesInput = ' '.join(uniqUniprotIds)
		
		paramsStep2 = {
			'from':'ACC',
			'to':'ENSEMBL_TRS_ID',
			'format':'tab',
			'query':uniprotValuesInput
			}
		
		data = urllib.urlencode(paramsStep2)
		request = urllib2.Request(url, data)
		request.add_header('User-Agent', 'Python')
		response = urllib2.urlopen(request)
		page = response.read()
		lines = page.split('\n')
		
		first = True
		
		for line in lines:
			if first==False and len(line)>0:
				uniprot, transcriptId = line.split('\t')
				
				if uniprot not in uniprotToTranscript:
					uniprotToTranscript[uniprot]=set()
					
				uniprotToTranscript[uniprot].add(transcriptId)

			else:
				first=False
		
		
		for uniprot in uniProtEntries:
			
			if uniprot in uniprotToTranscript:
				transcriptIds.append(';'.join(uniprotToTranscript[uniprot]))
			else:
				transcriptIds.append('')
		
			self.l.log('ensembl mapping ' + uniprot + ' -> ' + transcriptIds[-1] )
		

		# jesli dla bialka nie ma informacji o transkryptach pomijamy je w dalszej analizie zapisujac informacje o tym w logu 	
		for c in reversed(range(0, len(transcriptIds))):
			if '' == transcriptIds[c]:
				self.l.error('no transcript found, deleting ' + pdbIds[c] + ':' + uniProtEntries[c] + ' ' + str(chainsNos[c]) )
				del transcriptIds[c]
				del pdbIds[c]
				del uniProtEntries[c]
				del chainsNos[c]
		
		
		return [pdbIds, uniProtEntries, transcriptIds, chainsNos]