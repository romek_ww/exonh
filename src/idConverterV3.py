import SOAPpy
from SOAPpy import WSDL
from customLogger import Logger
import urllib,urllib2
import requests, sys

class IdConverter:

	l = Logger()

	def convert(self, pdbIdsIn):
	
		pdbIds = []
		chainsNos = []
		uniProtEntries = []
		transcriptIds = []
		
		# tlumaczenie PDB ID -> UniProt Accession
		
		self.l.log('tlumaczenie PDB ID -> UniProt Accession')

		requestUrl = 'http://www.bioinf.org.uk/cgi-bin/pdbsws/query.pl?plain=1&qtype=pdb&id='
		
		for pdbId in pdbIdsIn:
			r = requests.get(requestUrl+pdbId)
		
			if not r.ok:
					self.l.error("ERROR mapping pdbId for: " + pdbId)
					continue
			
			resp = r.text			
			
			chainSections = resp.split('//')

			for chainSection in chainSections:
				pdbIdn = 'NA'
				uniprotIdn = 'NA'
				chainIdn = 'NA'
				for line in chainSection.split('\n'):
					lineVals = line.split(': ')
					if len(lineVals)==2:
						if lineVals[0]=='PDB':
							pdbIdn=lineVals[1]
						if lineVals[0]=='AC':
							uniprotIdn=lineVals[1]
						if lineVals[0]=='CHAIN':
							chainIdn=lineVals[1]
				if pdbIdn!='NA ' and uniprotIdn!='NA' and chainIdn!='NA':
					self.l.log('uniProt mapping ' + pdbIdn + '->' + uniprotIdn + '->' + chainIdn )
					pdbIds.append(pdbIdn)
					uniProtEntries.append(uniprotIdn)
					chainsNos.append(chainIdn)
				

		# tlumaczenie UniProt Accession -> Ensembl Transcript ID
		self.l.log('tlumaczenie UniProt Accession -> Ensembl Transcript ID')

		url = 'http://www.uniprot.org/mapping/'
		
		uniprotToTranscript={}
		
		uniqUniprotIds = set(uniProtEntries)
		uniprotValuesInput = ' '.join(uniqUniprotIds)
		
		paramsStep2 = {
			'from':'ACC',
			'to':'ENSEMBL_TRS_ID',
			'format':'tab',
			'query':uniprotValuesInput
			}
		
		data = urllib.urlencode(paramsStep2)
		request = urllib2.Request(url, data)
		request.add_header('User-Agent', 'Python')
		response = urllib2.urlopen(request)
		page = response.read()
		lines = page.split('\n')
		
		first = True
		
		for line in lines:
			if first==False and len(line)>0:
				uniprot, transcriptId = line.split('\t')
				
				if uniprot not in uniprotToTranscript:
					uniprotToTranscript[uniprot]=set()
					
				uniprotToTranscript[uniprot].add(transcriptId)

			else:
				first=False
		
		
		for uniprot in uniProtEntries:
			
			if uniprot in uniprotToTranscript:
				transcriptIds.append(','.join(uniprotToTranscript[uniprot]))
			else:
				transcriptIds.append('')
		
			self.l.log('ensembl mapping ' + uniprot + ' -> ' + transcriptIds[-1] )
		

		# jesli dla bialka nie ma informacji o transkryptach pomijamy je w dalszej analizie zapisujac informacje o tym w logu 	
		for c in reversed(range(0, len(transcriptIds))):
			if '' == transcriptIds[c]:
				self.l.error('no transcript found, deleting ' + pdbIds[c] + ':' + uniProtEntries[c] + ' ' + str(chainsNos[c]) )
				del transcriptIds[c]
				del pdbIds[c]
				del uniProtEntries[c]
				del chainsNos[c]
		
		
		return [pdbIds, uniProtEntries, transcriptIds, chainsNos]