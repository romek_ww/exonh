from os import listdir
import time
from os.path import isfile, join
from idConverterV3 import IdConverter
from transcriptsComparator import TranscriptsComparator
from customLogger import Logger
from exonInfo2 import ExonInfo
import requests, sys
from pdbInfo import PdbInfo
from assignExonDomains import AssignExonDomains
from hydro import Hydro
from ExonBlast import ExonBlast
import ConfigParser
from exonGrouper import ExonGrouper
import os, sys
from blastDbMaker import BlastDBMaker
from sasaCalculator import SasaCalculator
import csv
from StatisticCollector import Collector
from sasaToExons import SasaToExons


def run():
	startTimeTotal = time.time()
	l = Logger()

	#inicjalizacja (czytanie pliku z konfiguracja)
	config = ConfigParser.RawConfigParser()
	config.read('config.cfg')
	# parametry dla hydrofobowosci:
	cutoff = config.getfloat('hydro', 'cutoff')
	table = config.get('hydro', 'table_name')
	dot = config.getfloat('hydro', 'dot')
	notgaussStr = config.get('hydro', 'notgauss')
	#parametry dla wyszukiwania exonow podobnych
	threadsCnt = config.get('blast', 'threadsCnt')
	thresholdEval = config.getfloat('blast', 'thresholdEval')
	#parametry modulu liczacego solvent accesible surface area
	dsspExecutable = config.get('sasa', 'dsspExecutable')
	#inne
	lenThreshold = config.getint('other', 'lenThreshold')
	skipWithNoExons = config.get('other', 'skipWithNoExons')
	
	createOuputDirectories()
	
	#przeczytanie pdbId z katalogu

	path = 'pdb'
	files = [ f for f in listdir(path) if isfile(join(path,f)) ]

	pdbIds=[]

	for f in files:
		if f[-3:].lower()=='pdb':  # tylko pliki z rozszezeniem pdb
			pdbIds.append(f[:-4].upper())

	l.log('read pdb: ' + str(pdbIds))

	#konwersja id-kow
	
	startTimeIdConv = time.time()

	idConv = IdConverter()

	pdbIds, uniProtEntries, transcriptIds, chainsNos = idConv.convert(pdbIds)

	l.logF(pdbIds)
	l.logF(uniProtEntries)
	l.logF(transcriptIds)
	l.logF(chainsNos)
	
	timeIdConv = time.time() - startTimeIdConv
	
	# dopasowanie najlepszego transkryptu:

	startTimeMainLoop = time.time()
	
	comparator = TranscriptsComparator()
	ei = ExonInfo()

	#lista wszystkich exonow
	exonList=[]
	#lista wszystkich domen pfam
	pfamList=[]

	#mapa zawierajaca dla pdb (ex. 1A6Q_A) dwuelementowa liste
	#sekwencji pdb i sekwencji najlepszego transkryptu (dopasowanie)
	assigns = {}
	
	#mapa ontologi: pdb_id -> ontologie
	ontologies = {}
	
	pdbInfo = PdbInfo()
	sasa = SasaCalculator(dsspExecutable)
	sasaToExons = SasaToExons()
	
	exonsForIsoformTranscripts = {}
	allExonsForBestTrn = {}
	
	for x in range(0, len(pdbIds)):
		pdb=pdbIds[x]

		transcriptStr=transcriptIds[x]

		try:
			best, assign = comparator.findBest(pdb, transcriptStr, chainsNos[x])
			
			assigns[pdb + '_' + chainsNos[x]] = assign
			l.log('best: ' + best + ' for: ' + pdb + '_' + chainsNos[x], Logger.GREEEN)
			
			if best=='':
				continue
			
			# pobranie informacji o exonach dla transkryptow
			
			protData = [pdb, uniProtEntries[x], chainsNos[x]]
			transcriptsExons = ei.getExons(transcriptStr, protData)
			
			#mapa: klucz to id transkryptu referencyjnego wartosc to mapa gdzie kluczem jest id transkryptu-izoformy a wartoscia lista exonow (wlacznie z referencyjnym)
			exonsForIsoformTranscripts[best]=transcriptsExons
			
			bestExons = transcriptsExons[best]
			
			l.log('exons for ' + best + ': ' + str(len(bestExons)), Logger.BLUE)
			
				
			#pobieramy powiazane domeny pfam i informacje o ontologiach


			l.log('pobieranie ontologi i domen dla pdb')

			if not pdb in ontologies:
				ontologiesTmp, pfamDomains = pdbInfo.getInfo(pdb)
				ontologies[pdb] = ontologiesTmp
				pfamList = pfamList + pfamDomains
				l.log('ontologies for ' + pdb + ': ' + str(len(ontologiesTmp)), Logger.YELLOW)
				l.log('pfam domains for ' + pdb + ': ' + str(len(pfamDomains)), Logger.YELLOW)

				
			# znajdujemy domeny pfam  z ktorymi pokrywaja sie exony

			l.log('dopasowywanie exonow do domen')

			for domain in pfamDomains:
				domain.printInfo()
			
			assignExonDomains = AssignExonDomains(pfamDomains, assigns, lenThreshold)

			bestExons = assignExonDomains.assign(bestExons)
			
			l.log('exons for ' + best + ' after filtering: ' + str(len(bestExons)), Logger.BLUE)
			
			if len(bestExons)==0 and skipWithNoExons=='Y':
				l.log('no exons, skipping hydro calculation', Logger.BLUE)
				continue
		
			# liczymy hydrofobowosci dla residuow w pdb
			
			if notgaussStr=='N':
				notgauss = False
			else:
				notgauss = True
			
			l.log('calculate hydro')	
			
			pdbFile = './pdb/'+pdb+'.pdb'
			
			hdr = Hydro(pdbFile, table, cutoff, dot, notgauss)
		
			hydroChain, folderName, sum_raw_ca, sum_raw_g = hdr.start(chainsNos[x])
		
			#hydroChain - lista aminokwasow z pdb_no po ktorym musimy matchowac
			
			matched = matchHydroToExons(hydroChain, bestExons)
			
			#zapisanie na dysk hydrofobowosci z dopasowanymi numerami exonow
			writeMatchedCsv(matched,folderName, pdb, chainsNos[x])
			
			#grupowanie danych dla eksonow
			grouper = ExonGrouper()
			grouped = grouper.run(matched, folderName, pdb, chainsNos[x])
			
			l.logF('EXON HYDRO: ' + str(grouped))
			
			l.log('hydro done')
			
			for exon in bestExons:
				#pomijamy exony z UTR [0, 0] oraz brakujace w pliku pdb[-1, -1] oraz z lanuchow dla ktorych nie policzylismy hydrofobowosci
				#pierwsze 2 warunki istotne tylko jesli minimalna dlugosc exona nie ustawiona wpp zostana wczesniej odrzucone
				l.log('przetwazany exon: ' + exon.getExonId() + " start: " + str(exon.getStartPdb()) + ' end: ' + str(exon.getEndPdb()))
				if exon.getStartPdb()!=0 and exon.getEndPdb()!=0 and exon.getStartPdb()!=-1 and exon.getEndPdb()!=-1 and exon.getExonId() in grouped:
					exon.saveExonSequence()
					chainGrouped = grouped
					l.log('---looking for: ' + exon.getExonId() + " in " + exon.getChain())
					isCoreCA = chainGrouped[exon.getExonId()][8]
					isCoreG = chainGrouped[exon.getExonId()][9]
					exon.setIsCoreCA(isCoreCA)
					exon.setIsCoreG(isCoreG)
					exon.setHydroValCA(chainGrouped[exon.getExonId()][4])
					exon.setOtCA(chainGrouped[exon.getExonId()][0])
			
			# wyliczenie SASA dla residuuw w PDB
			# zwraca liste krotek z wartosciami, nigdzie nie zapamietywane narazie
			sasaOut = sasa.perform(pdb);
			sasaToExons.assign(bestExons, sasaOut, chainsNos[x])
			
			l.log('BEST EXONS: ')
			for b in bestExons:
				b.printInfo()
			
			exonList = exonList + bestExons
			allExonsForBestTrn[best] = bestExons
		except RuntimeError as msg:
			l.error('ERROR processin file ' + pdbFile + ', message: ' + str(msg))
	
	timeMainLoop = time.time() - startTimeMainLoop
	
	startTimeSimilarSearch = time.time()
	
	# tworzenie bazy danych z sekwencjami exonow
	bdm=BlastDBMaker()
	bdm.make()
	
	#wyszukiwanie exonow podobnych
	
	eb = ExonBlast(threadsCnt, thresholdEval)
	
	for exon in exonList:
		if exon.getStartPdb()!=0 and exon.getEndPdb()!=0 and exon.getStartPdb()!=-1 and exon.getEndPdb()!=-1 and exon.getSequenceFile()!='':
			eb.blast(exon)
			
			for similar in exon.getSimilarExons():
				similar.printInfo()
	
	# zapisanie informacji do plikow
	
	timeSimilarSearch = time.time() - startTimeSimilarSearch
	
	writeAllData(exonList, pfamList, ontologies)
	
	#wykresy statystyki
	
	startTimeStats = time.time()
	
	sc = Collector(exonList, exonsForIsoformTranscripts, allExonsForBestTrn)
	sc.showCoreExonsHydro()
	sc.coreExonMultiplicity()
	sc.generateIsoformReport()
	sc.distributionCharts()
	sc.calculateExonsComposition()
	
	timeStats = time.time() - startTimeStats
	
	l.log("STATISTICS:", Logger.GREEEN)
	l.log("Id conversion time: %d min %d sec" % divmod(timeIdConv, 60), Logger.GREEEN)
	l.log("Main loop time: %d min %d sec" % divmod(timeMainLoop, 60), Logger.GREEEN)
	l.log("Similar search time: %d min %d sec" % divmod(timeSimilarSearch, 60), Logger.GREEEN)
	l.log("Stats time: %d min %d sec" % divmod(timeStats, 60), Logger.GREEEN)
	l.log("Total time: %d min %d sec" % divmod((time.time() - startTimeTotal), 60), Logger.GREEEN)
	l.log("PDB processed: " + str(len(pdbIds)), Logger.GREEEN)
	
#zwraca przesuniecia id (np. jesli numeracja w pdb zaczyna sie od 1 zwraca 0)
def getOffsetForPdbChain(chain):
	if len(chain)>0:
		firsRecordNo = chain[0][1]
		return firsRecordNo-1
	else:
		return 0

def matchHydroToExons(hydroChain, exons):
	hydro = hydroChain
	hydro = sorted(hydro, key=lambda aminoAcid: aminoAcid[1])	
	#offset = getOffsetForPdbChain(hydro)
	#print 'offset: ' + str(offset)
		
	hydroWithExon = []
	
	for exon in exons:
		#exonStart = exon.getStartPdb() + offset
		#exonEnd = exon.getEndPdb() + offset
		exonStart = exon.getStartPdb()
		exonEnd = exon.getEndPdb()
		exonId=exon.getExonId()
		
		counter = 0
		for aminoAcid in hydro:
			counter = counter + 1
			#pdbNo = aminoAcid[1]
			#if pdbNo<=exonEnd and pdbNo>=exonStart: - mathowanie po pdbNo (counter niepotrzebny)
			if counter<=exonEnd and counter>=exonStart:
				hydroWithExon.append(aminoAcid + (exonId,))
		
	return hydroWithExon
	
def writeMatchedCsv(data,folderName, protein, chainIdn):
	header = [('RES', 'PDB_NO', 'HYDRO_CA', 'HYDRO_G', 'THEO_CA', 'THEO_G', 'EXON_ID')]
	ret=data
	ret = header + ret
	fileName = protein + '_' + chainIdn + '_exons.csv'
	outputfile = os.path.join(folderName,fileName)
	with open(outputfile, 'wb') as f:
		writer = csv.writer(f)
		writer.writerows(ret)	

# zapisa danych 'exonowych' do pliku
def writeAllData(exons, pfamDomains, ontologies):

	exonHeader = [('EXON ID', 'transcriptId', 'pdbId', 'chain', 'isCoreCA', 'isCoreG', 'start', 'end', 'startAA', 'endAA', 'startPdb', 'endPdb', 'pfamIds', 'similarExons(score)')]
	ontologiesHeader = [('Ontology ID', 'PDB ID', 'Chain', 'Category', 'Name', 'Description', 'Synonyms')]
	pfamHeader = [('PFAM ID', 'PDB ID', 'Chain', 'Name', 'pdbStart', 'pdbEnd', 'eValue', 'description')]

	exonFileName = 'exonsData.csv'
	ontologiesFileName = 'ontologiesData.csv'
	pfamFileName = 'pfamDomainsData.csv'
	
	for exon in exons:
		exonHeader.append(exon.getFullData())
		
	with open(exonFileName, 'wb') as f:
			writer = csv.writer(f)
			writer.writerows(exonHeader)	

	for domain in pfamDomains:
		pfamHeader.append(domain.getFullData())
		
	with open(pfamFileName, 'wb') as f:
			writer = csv.writer(f)
			writer.writerows(pfamHeader)

	for pdbId, pdbOntologies in ontologies.iteritems():
		for ontology in pdbOntologies:
			ontologiesHeader.append(ontology.getFullData())
		
	with open(ontologiesFileName, 'wb') as f:
			writer = csv.writer(f)
			writer.writerows(ontologiesHeader)			
			
# tworzy katalogi na dane wyjsciowe			
def createOuputDirectories():
	if not os.path.exists('./transcripts'):
		os.makedirs('./transcripts')
	if not os.path.exists('./hydro'):
		os.makedirs('./hydro')
	if not os.path.exists('./exons'):
		os.makedirs('./exons')
	if not os.path.exists('./sasa'):
		os.makedirs('./sasa')		
	if not os.path.exists('./cache'):
		os.makedirs('./cache')		
			
if __name__ == '__main__':
    run()			