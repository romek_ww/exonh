from customLogger import Logger

class IsoformData:

	l = Logger()

	def __init__(self, isoformId, isoformExons, isoformAllExons):
		self.isoformId = isoformId
		self.isoformExons = isoformExons
		self.isoformAllExons = isoformAllExons
		if isoformAllExons!='NA':
			self.isoformCoreExons = [ex for ex in isoformAllExons if ex.isCoreCa()==True]
		else:
			self.isoformCoreExons='NA'
			
		self.isoformExonsIds = [ex.getExonId() for ex in isoformExons]
		
		if self.isoformCoreExons!='NA':
			self.isoformCoreExonsIds = [ex.getExonId() for ex in self.isoformCoreExons]
		else:
			self.isoformCoreExonsIds='NA'
		
	# id izoformy (transkryptu)
	def getIsoformId(self):
		return self.isoformId
		
	# lista wszystkich exonow skladajacych sie na transktypt
	def getIsoformExons(self):
		return self.isoformExons
		
	# lista exonow centrum trankryptu
	# tylko w przypadku jesli znamy struktore, wpp puste
	def getIsoformCoreExons(self):
		return self.isoformCoreExons
	
	# lista id wszystkich exonow skladajacych sie na transktypt
	def getIsoformExonsIds(self):
		return self.isoformExonsIds
		
	# lista id exonow centrum trankryptu
	# tylko w przypadku jesli znamy struktore, wpp puste
	def getIsoformCoreExonsIds(self):
		return self.isoformCoreExonsIds
	
	def getIsoformAllExons(self):
		return self.isoformAllExons
	
	def printInfo(self):
		self.l.logF("isoformId: " + self.isoformId + ', isoformExons: ' + str(self.isoformExonsIds) + " isoformCoreExons: " + str(self.isoformCoreExonsIds))
		
		