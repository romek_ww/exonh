import requests, sys
from exon import Exon
from customLogger import Logger

class ExonInfo:

	l = Logger()
	
	def getExons(self, transcriptIds, protData):

		urlTemplate = \
		'''http://ensembl.org/biomart/martservice?query=''' \
		'''<?xml version="1.0" encoding="UTF-8"?>''' \
		'''<!DOCTYPE Query>''' \
		'''<Query  virtualSchemaName = "default" formatter = "TSV" header = "0" uniqueRows = "0" count = "" datasetConfigVersion = "0.6" >		''' \
		'''<Dataset name = "hsapiens_gene_ensembl" interface = "default" >''' \
		'''<Filter name = "ensembl_transcript_id" value = "%s"/>''' \
		'''<Attribute name = "ensembl_transcript_id" />''' \
		'''<Attribute name = "ensembl_exon_id" />''' \
		'''<Attribute name = "cds_start" />''' \
		'''<Attribute name = "cds_end" />''' \
		'''</Dataset>''' \
		'''</Query>'''
	
		url = urlTemplate % transcriptIds
		
		r = requests.get(url)

		if not r.ok:
			r.raise_for_status()
			sys.exit()
					 
		trsOut = r.text
		
		mapForTranscript = {}
		
		for transcriptId in transcriptIds.split(','):
			mapForTranscript[transcriptId] = []
		
		for line in trsOut.split('\n'):
			sp = line.split('\t')
			#jesli exon ma puste wspolrzedne w sekwencji CDS
			#oznacza to ze caly znajduje sie w sekcji UTR - pomijamy go
			if len(sp)==4 and sp[2]!='' and sp[3]!='':
				trnId = sp[0].encode('utf-8')
				exonObj = Exon(sp[1].encode('utf-8'), sp[2].encode('utf-8'), sp[3].encode('utf-8'), trnId, *protData)
				mapForTranscript[trnId].append(exonObj)


		self.l.log('Pobrane exony: ' + str(mapForTranscript))
		
		return mapForTranscript