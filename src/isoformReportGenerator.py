from customLogger import Logger
from isoformData import IsoformData
import os

class IsoformReportGenerator:

	l = Logger()

	def generateReport(self, exonsForIsoformTranscripts, allExonsForBestTrn):
	
		#usuniecie starego raportu jesli istnieje
		if os.path.exists('./isoformsReport.html'):
			os.remove('./isoformsReport.html')
			
		f = open('./isoformsReport.html', 'w')	
		
		self.l.log('izoformy - generacja raportu')
		
		self.writeHtmlBegin(f)
		
		for refID, isoformMap in exonsForIsoformTranscripts.iteritems():
			refExons = isoformMap[refID]
			
			if len(refExons)==0 or refID not in allExonsForBestTrn:
				self.l.log('no refExons for: ' + refID)
				continue
			
			pdbId = refExons[0].getPdbId()+'_'+refExons[0].getChain()
			
			self.l.log('generating for referential: ' + refID + " (" + pdbId + ")")
			
			refAllExons = allExonsForBestTrn[refID]
			
			#refCoreExons = [ex for ex in refAllExons if ex.isCoreCa()==True] #ta linia jesli na raporcie tylko exony centrum izoformy referencyjnej
			refCoreExons = refAllExons #ta linia jesli na raporcie wszystkie exony izoformy referencyjnej (nazwa zmiennej mylaca...)
			
			isoforms = []
			
			for isoformId, exonList in isoformMap.iteritems():
				#TODO usuwac referencyjna
				
				if isoformId in allExonsForBestTrn:
					#znamy strukture dla izoformy (mamy pdb)
					isoformAllExons = allExonsForBestTrn[isoformId]
				else:
					#nie znamy struktury (zapewne wiekszosc przypadkow)
					isoformAllExons='NA'
				
				isoform = IsoformData(isoformId, exonList, isoformAllExons)
				isoforms.append(isoform)
				
			self.generateReportSegment(f, refID, pdbId, refCoreExons, isoforms)
		
		self.writeHtmlEnd(f)

	def generateReportSegment(self, file, refId, pdbId, refCoreExons, isoforms):
		htmlSegment = self.getHtml(refId, pdbId, refCoreExons, isoforms)
		file.write(htmlSegment)
		
		
	def getHtml(self, refId, pdbId, refCoreExons, isoforms):
		htmlOut="<h1>REF ID: " + refId + "</h1><h2> (pdb: " + pdbId + ")</h2>\n"
		
		refCoreExonsIds = [ex.getExonId() for ex in refCoreExons]
		
		if len(isoforms)==0:
			htmlOut+="<br/>No isoforms<br/><br/><br/>\n"
		else:
		
			htmlOut += "<table border='1px'>\n"
			htmlOut += "<tr><th>isoform</th><th>ref exon</th><th>exist</th><th>is core</th><th>is core ref</th>"
			htmlOut += "<th>avg. solvent acc. surface comp.</th><th>secondary structure comp.</th></tr>\n"
			
			for isoform in isoforms:
				htmlOut += "<tr><td align='top'>\n"
				htmlOut += isoform.getIsoformId() + "\n"
				htmlOut += "</td><td>    \n"
				
				for core in refCoreExonsIds:
					htmlOut += "<p>" + core + "</p><br/>"
				
				#ostatni <br/> niepotrzebny ucinamy
				htmlOut = htmlOut[:-5]	
				htmlOut += "</td><td>    \n"

				isoformExonsIds = isoform.getIsoformExonsIds()
				
				for core in refCoreExonsIds:
					if core in isoformExonsIds:
						htmlOut += "<p class='green'>Y</p><br/>"
					else:
						htmlOut += "<p class='red'>N</p><br/>"
				
				htmlOut = htmlOut[:-5]				
				htmlOut += "</td><td>    \n"

				
				for core in refCoreExons:
					if core.isCoreCa()==True:
						htmlOut += "<p class='green'>Y</p><br/>"
					else:
						htmlOut += "<p class='red'>N</p><br/>"
				
				htmlOut = htmlOut[:-5]				
				htmlOut += "</td><td>    \n"
				
				
				isoformCoreExonsIds = isoform.getIsoformCoreExonsIds()
				
				for core in refCoreExonsIds:
					if 'NA'==isoformCoreExonsIds:
						htmlOut += "<p>-</p><br/>"
					else:
						if core in isoformCoreExonsIds:
							htmlOut += "<p class='green'>Y</p><br/>"
						else:
							htmlOut += "<p class='red'>N</p><br/>"
				
				htmlOut = htmlOut[:-5]
				htmlOut += "</td><td>    \n"
				
				isoformAllExons = isoform.getIsoformAllExons()
				
				secStrCompList = []
				
				#porownanie powierzchni dostepnej dla rozpuszlanika dla danego exonu w transkrypcie referencyjnym i izoformie
				for core in refCoreExons:
					#filtrujemy ten sam exon z izoformy
					selectedExonLst = [ex for ex in isoformAllExons if ex.getExonId()==core.getExonId()] if isoformAllExons!='NA' else []
					if isoformCoreExonsIds!='NA' and len(selectedExonLst)>0:
						#znamy struktore izoformy oraz wystepuje w niej ten sam exon - porownanie
						selectedExon = selectedExonLst[0]
						dateStrSolv = self.getSolvSurfComp(core, selectedExon)
						dataStrSec = self.getSecStrComp(core, selectedExon)
						secStrCompList.append(dataStrSec)
						htmlOut += "<p>" + dateStrSolv + "</p><br/>"
					else:
						#nie znamy struktory lub exon nie wystepuje w tej izoformie - wartosci tylko dla exonu z transkryptu referencyjnego
						dateStrSolv = self.getSolvSurfComp(core, 'NA')
						dataStrSec = self.getSecStrComp(core, 'NA')
						secStrCompList.append(dataStrSec)
						htmlOut += "<p>" + dateStrSolv + "</p><br/>"
				
				htmlOut = htmlOut[:-5]
				htmlOut += "</td><td>    \n"
				
				#porownanie udzialu danego typu struktury drugorzedowej kodowanej przez exon w transkrypcie referencyjnym i izoformie
				
				for secStrComp in secStrCompList:
					htmlOut += "<p>" + secStrComp + "</p><br/>"
					
				htmlOut = htmlOut[:-5]
				htmlOut += "</td></tr>\n"
				
			htmlOut += "</table><br/><br/>\n"
		
		return htmlOut
		
	def writeHtmlBegin(self, file):
		file.write("<html><head>\n")
		file.write("<link rel='stylesheet' type='text/css' href='isoformReportStyles.css'>\n")
		file.write("</head><body>\n")
	
	def writeHtmlEnd(self, file):
		file.write("</body></html>")
		
	def getSolvSurfComp(self, refExon, selectedExon):
		refCoreAcc = refExon.getAvgSolventRelAccAsStr()
		if selectedExon!='NA':
			selectedAcc = selectedExon.getAvgSolventRelAccAsStr()
		else:
			selectedAcc = '-'
			
		return refCoreAcc + " / " + selectedAcc
	
	def getSecStrComp(self, refExon, selectedExon):
		refCoreStr = refExon.getSecStrSharesAsStr()
		if selectedExon!='NA':
			selectedStr = selectedExon.getSecStrSharesAsStr()
		else:
			selectedStr = '-'
			
		return refCoreStr + " / " + selectedStr
	