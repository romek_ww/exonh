from customLogger import Logger
import math
from StringIO import StringIO
from Bio.Blast import NCBIXML
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO
import os.path

class Exon:

	l = Logger()

	def __init__(self, exonId, start, end, transcriptId, pdbId, uniprotId, chain):
		self.exonId = exonId
		self.transcriptId = transcriptId
		self.pdbId = pdbId
		self.start = start
		self.end = end
		self.uniprotId = uniprotId
		self.chain = chain
		self.pfamDomains=[]
		self.startAA, self.endAA = self.calculateAACoordinates(start, end);
		self.startPdb=0
		self.endPdb=0
		self.sequenceFile = ''
		self.isCoreCA = 'NA'
		self.hydroValCA = 'NA'
		self.isCoreG = 'NA'
		self.similarExons = []
		self.avgSolventRelAcc = 'NA'
		self.secStrShares = 'NA'
		self.secStrComposition = {}
		self.otCA = 'NA'
		self.aaSeq = ''
		
	# id exonu
	def getExonId(self):
		return self.exonId
		
	# id transktyptu do ktorego nalezy exon	
	def getTranscriptId(self):
		return self.transcriptId
		
	# pdb id bialka ktorego jest transkrypt
	def getPdbId(self):
		return self.pdbId
		
	# wspolrzeedne poczatku exonu w transkrypcie	
	def getStart(self):
		return self.start
	
	# wspolrzeedne konca exonu w transkrypcie	
	def getEnd(self):
		return self.end
	
	# wspolrzeedne poczatku exonu w bialku otrzymanym z przetlumaczenia transkryptu
	def getStartAA(self):
		return self.startAA
	
	# wspolrzeedne konca exonu w bialku	otrzymanym z przetlumaczenia transkryptu
	def getEndAA(self):
		return self.endAA
		
	# wspolrzeedne poczatku exonu w bialku z PDB
	def getStartPdb(self):
		return self.startPdb
	
	# wspolrzeedne konca exonu w bialku	z PDB
	def getEndPdb(self):
		return self.endPdb
		
	def setStartPdb(self, value):
		self.startPdb = value
	
	def setEndPdb(self, value):
		self.endPdb = value
	
	# uniprotId bialka (jesli bialko ma kilka lancochow beda one mialy to samo pdbId i
	# rozne uniprotId)
	def getUniprotId(self):
		return self.uniprotId
	
	# lancuch
	def getChain(self):
		return self.chain
		
	# zwraca domeny
	def getDomains(self):
		return self.pfamDomains
		
	# zwraca liste podobnych exonow
	def getSimilarExons(self):
		return self.similarExons	

	# zwraca nazwe pliku z sekwencja NN exonu
	def getSequenceFile(self):
		return self.sequenceFile
		
	# dodaje domene pfam do exonu
	def addDomain(self, domain):
		self.pfamDomains.append(domain)

	# dodaje exon do listy podobnych
	def addSimilarExon(self, similarExon):
		self.similarExons.append(similarExon)
		
	def setEndAA(self, value):
		self.endAA = value
	
	# czy exon jest exonem centrum dla hydrofobowosci liczonej w punkcie CA aminokwasu
	def isCoreCa(self):
		return self.isCoreCA
		
		
	# wartosc hydrofobowosci dla exonu liczonej w punkcie CA aminokwasow
	def getHydroValCA(self):
		return self.hydroValCA	

	# wartosc O/T (liczone w punkcie CA aminokwasow)
	def getOtCA(self):
		return self.otCA	
		
	# czy exon jest exonem centrum dla hydrofobowosci liczonej w centrum geometrycznym aminokwasow
	def isCoreG(self):
		return self.isCoreG
		
	def setIsCoreCA(self, value):
		self.isCoreCA = value	
		
	def setHydroValCA(self, value):
		self.hydroValCA = value	
		
	def setOtCA(self, value):
		self.otCA = value		

	def setIsCoreG(self, value):
		self.isCoreG = value	
		
	def setAvgSolventRelAcc(self, value):
		self.avgSolventRelAcc = value
		
	def setSecStrShares(self, value):
		self.secStrShares = value	
		
	# srednia procentowa dostepnosc powieszchni dla rozpuszlanika dla aminokwasow wchodzacych w sklad exonu
	def getAvgSolventRelAcc(self):
		return self.avgSolventRelAcc
		
	# mapa zawirajaca procentowe udzialy danego typu struktur drugorzedowych w exonie (z dssp struktura drugorzedowa per aminokwas)	
	def getSecStrShares(self):
		return self.secStrShares

	def setSecStrComposition(self, value):
		self.secStrComposition = value

	def getSecStrComposition(self):
		return self.secStrComposition

	def setAaSec(self, value):
		self.aaSeq = value

	def getAaSec(self):
		return self.aaSeq
	
	def getAvgSolventRelAccAsStr(self):
		if self.avgSolventRelAcc!='NA':
			percentValue = self.avgSolventRelAcc*100
			formatedStr = "%.2f" % percentValue
			return formatedStr.rstrip("0") + "%"
		else:
			return 'NA'
	
	def getSecStrSharesAsStr(self):
		if self.secStrShares!='NA':
			outStr = ""
			for key, value in self.secStrShares.iteritems():
				percentValue = value*100
				formatedStr = "%.2f" % percentValue
				keyStr = key
				if keyStr=='-':
					keyStr='N' #None
				outStr = outStr + ', ' + keyStr + " - " + formatedStr.rstrip("0") + "%"
				
			return 	outStr[2:]
		else:
			return 'NA'
	
	def printInfo(self):
		self.l.log("EXON ID: " + self.exonId + ', from transcript: ' + self.transcriptId + ', pdb: ' + self.pdbId + ', uniprot id: ' + self.uniprotId + ', chain: ' + self.chain + ', isCoreCA: ' + str(self.isCoreCA) + ', isCoreG: ' + str(self.isCoreG), Logger.GREEEN)
		self.l.logF("START: " + self.start + ', END: ' + self.end + " startAA: " + str(self.startAA) + ', endAA: ' + str(self.endAA) + " startPdb: " + str(self.startPdb) + ', endPdb: ' + str(self.endPdb) + ', pfam domains count: ' + str(len(self.pfamDomains)))
		self.l.logF("avgSolventRelAcc: " + self.getAvgSolventRelAccAsStr() + ", secStrShares: " + self.getSecStrSharesAsStr())
		
	def getFullData(self):
		
		#narazie bez danych z dssp
		
		pfamStr = ''
		similarExonstStr = ''
		
		for domain in self.pfamDomains:
			if pfamStr == '':
				pfamStr = domain.getPfamAcc()
			else:
				pfamStr = pfamStr + ',' + domain.getPfamAcc()

		for ex in self.similarExons:
			if similarExonstStr == '':
				similarExonstStr = ex.getExonId() + '(' + str(ex.getTotalScore()) + ')' 
			else:
				similarExonstStr = similarExonstStr + ',' + ex.getExonId() + '(' + str(ex.getTotalScore()) + ')' 
				
		return (self.exonId, self.transcriptId, self.pdbId, self.chain, self.isCoreCA, self.isCoreG, self.start, self.end, self.startAA, self.endAA, self.startPdb, self.endPdb, pfamStr, similarExonstStr)
	
	def calculateAACoordinates(self, start, end):
		startAA = int(math.ceil(int(start)/3.0))
		endAA = int(math.ceil(int(end)/3.0))
		return [startAA, endAA]
		
		
	def saveExonSequence(self):
		file = './transcripts/' + self.transcriptId + '_NN.fasta'
		aaFile = './transcripts/' + self.transcriptId + '.fasta'
		targetFile = './exons/' + self.exonId + '.fasta'
		
		self.sequenceFile = './exons/' + self.exonId + '.fasta'
		
		if not os.path.isfile(targetFile):
			file = SeqIO.parse(open(file), "fasta")
			record = next(file)
			seqStr = str(record.seq)

			seqStr = seqStr[int(self.start):int(self.end)]
			
			seq = Seq(seqStr, IUPAC.unambiguous_dna)
			sRecord = SeqRecord(seq, id=self.exonId)
			
			output_handle = open(targetFile, "w")
			SeqIO.write(sRecord, output_handle, "fasta")

		# dla statystyk
		aaFile = SeqIO.parse(open(aaFile), "fasta")
		aaRecord = next(aaFile)
		aaSeqStr = str(aaRecord.seq)
		aaSeqStr = aaSeqStr[int(self.start):int(self.end)]
		self.aaSeq=aaSeqStr;
			
	def __repr__(self):
		return self.exonId
		
	def __eq__(self, other):
		if isinstance(other, Exon):
			return (self.exonId == other.exonId)
		else:
			return False
		  
	def __ne__(self, other):
		return (not self.__eq__(other))	
		
	def __hash__(self):
		return hash(self.__repr__())	
		
		