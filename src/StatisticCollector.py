import matplotlib.pyplot as plt
from customLogger import Logger
from isoformReportGenerator import IsoformReportGenerator
import numpy as np
import csv

class Collector:

	l = Logger()

	def __init__(self, exons, exonsForIsoformTranscripts, allExonsForBestTrn):
			self.exons = exons
			self.exonsForIsoformTranscripts = exonsForIsoformTranscripts
			self.allExonsForBestTrn = allExonsForBestTrn

	# rysuje wykres hydrofobowosci exonow z rozroznieniem na exony centrum i pozostale
	def showCoreExonsHydro(self):

		counter = 1

		oxCore = []
		oxNCore = []
		hydroCore = []
		hydroNCore = []

		# tylko dla Ca - zrobic parametryzacje albo skopiowac dla G

		for ex in self.exons:
			if ex.getHydroValCA()!='NA':

				if ex.isCoreCa() == True:
					oxCore.append(counter)
					hydroCore.append(float(ex.getHydroValCA()))
					self.l.log("dodaje Core: " + ex.getExonId() + " z wartoscia: " + str(ex.getHydroValCA()))
				else:
					oxNCore.append(counter)
					hydroNCore.append(float(ex.getHydroValCA()))
					self.l.log("dodaje NCore: " + ex.getExonId() + " z wartoscia: " + str(ex.getHydroValCA()))

				counter = counter + 1;
			else:
				self.l.log("hydrofobowosc NA for exonId: " + ex.getExonId())

		#zapisa danych wykresu do pliku
		dataCore = zip(oxCore, hydroCore, ['1']*len(oxCore))
		dataNcore = zip(oxNCore, hydroNCore, ['0']*len(oxNCore))
		data = dataCore + dataNcore
		self.writeChartDataToFile('exons_hydrophobicity.csv', data)

		plt.plot(oxCore, hydroCore, 'go', oxNCore, hydroNCore, 'ro')
		plt.title('exons hydrophobicity')
		plt.ylabel('hydrophobicity')
		plt.xlabel('exons')
		plt.show()

	#zbiera statystyki dla exonow centum zgrupowanych wedlug ich ilosci w lancuchu (zgodny pdbId i lancuch)
	def coreExonMultiplicity(self):

		# jak wyzej - tylko dla Ca
		coreExons = [ex for ex in self.exons if ex.isCoreCa()==True]
		self.l.log("core exons: " + str(len(coreExons)))
		data = {}
		pdbData = {}

		for exon in coreExons:
			count = len([i for i in coreExons if i.getPdbId()==exon.getPdbId() and i.getChain()==exon.getChain()])
			chainIdent = exon.getPdbId() + '_' + exon.getChain()

			if count in data:
				data[count].append(exon)
				if chainIdent not in pdbData[count]:
					pdbData[count].append(chainIdent)
			else:
				data[count] = [exon]
				pdbData[count] = [chainIdent]

		colLabels=[]
		multiplicity=[]

		for key, value in data.iteritems():
			colLabels.append(str(key))
			multiplicity.append(len(value)/key)

		#Create a figure and axes with room for the table
		fig = plt.figure()

		#Create labels for the rows and columns as tuples
		rowLabels = ('sr. dl. exonu centrum', 'sr. hydrofob. centrum', 'dl. centrum do wszyst.(%)', 'ilosc centrum do wszyst.(%)')

		#Table data as a numpy array
		tableData = []

		avlen = []
		avHydro = []
		lenToAll = []
		countToAll = []

		for key, value in data.iteritems():
			avlen.append(self.averageExonsLength(value)) # srednia dlugosc exonu centrum (AA)
			avHydro.append(self.averageHydro(value)) # srednia wartosc hydrofobowosci exonu centum
			lenToAll.append(self.coreToAllLen(value)) # sredni stosunek dlugosci exonow centrum do dlugosci calego bialka(%)
			countToAll.append(self.coreToAllCount(value)) # srednia ilosc exonow centrum do wszystkich exonow bialka(%)

		for key, value in pdbData.iteritems():
			self.l.log("core exons " + str(key) + ': ' + str(value))

		withNocore = self.countWithNoCoreExons(self.exons)

		if withNocore>0:
			multiplicity.insert(0,withNocore)
			colLabels.insert(0,"0")
			avlen.insert(0,"x")
			avHydro.insert(0,"x")
			lenToAll.insert(0,"x")
			countToAll.insert(0,"x")

		tableData.append(avlen)
		tableData.append(avHydro)
		tableData.append(lenToAll)
		tableData.append(countToAll)

		#zapis do pliku
		data = zip(colLabels, multiplicity, avlen, avHydro, lenToAll, countToAll)
		self.writeChartDataToFile('blur_of_hydrophobic_core.csv', data)

		#Show the table
		table = plt.table(cellText=tableData,
						  rowLabels=rowLabels,
						  colLabels=colLabels,
						  loc='bottom')

		#table.auto_set_font_size(False)
		#table.set_fontsize(14)

		ind = np.arange(len(multiplicity))
		plt.bar(ind, multiplicity, align='center', width=0.7)

		#Turn off x-axis ticks and show the plot
		fig.subplots_adjust(bottom=0.17, left=0.25)
		plt.xlim(xmin=-0.5)
		plt.xlim(xmax=len(multiplicity)-0.5)
		plt.xticks([])
		plt.title('Blur of hydrophobic core')
		plt.ylabel('number of proteins')
		plt.show()


	# generacja raportu o exonach centrum w izoformach
	def generateIsoformReport(self):
		irg = IsoformReportGenerator()
		irg.generateReport(self.exonsForIsoformTranscripts, self.allExonsForBestTrn)

	def averageExonsLength(self, exons):
		total = 0;

		for ex in exons:
			exLength = ex.getEndPdb()-ex.getStartPdb()
			total = total + exLength

		out = float(total)/len(exons)
		return self.format(out)

	def averageHydro(self, exons):
		total = 0;

		for ex in exons:
			total = total + float(ex.getHydroValCA())

		out = float(total)/len(exons)
		return self.format(out)

	def coreToAllLen(self, exons):

		totalPercent = 0

		for ex in exons:
			sameChainExons = [i for i in self.exons if i.getPdbId()==ex.getPdbId() and i.getChain()==ex.getChain()]
			sameChainCoreExons = [i for i in exons if i.getPdbId()==ex.getPdbId() and i.getChain()==ex.getChain()]
			sameChainExonsLen= sum(e.getEndPdb()-e.getStartPdb() for e in sameChainExons)
			sameChainCoreExonsLen = sum(e.getEndPdb()-e.getStartPdb() for e in sameChainCoreExons)
			totalPercent = totalPercent + (float(sameChainCoreExonsLen)/sameChainExonsLen*100)

		out = float(totalPercent)/len(exons)
		return self.format(out)

	def coreToAllCount(self, exons):
		totalPercent = 0

		for ex in exons:
			sameChainExons = len([i for i in self.exons if i.getPdbId()==ex.getPdbId() and i.getChain()==ex.getChain()])
			sameChainCoreExons = len([i for i in exons if i.getPdbId()==ex.getPdbId() and i.getChain()==ex.getChain()])
			totalPercent = totalPercent + (float(sameChainCoreExons)/sameChainExons*100)

		out = float(totalPercent)/len(exons)
		return self.format(out)

	def format(self, val):
		formatedStr = "%.5f" % val
		return formatedStr.rstrip("0")

	def format2(self, val):
		formatedStr = "%.2f" % val
		tmp = formatedStr.rstrip("0")

		if tmp[-1]=='.':
			tmp=tmp+'0';

		return tmp + "%"

	def countWithNoCoreExons(self, exons):
		noCore = []

		for ex in exons:
			if ex.isCoreCa()==False:
				count = len([i for i in exons if i.getPdbId()==ex.getPdbId() and i.getChain()==ex.getChain() and i.isCoreCa()==True])

				if count==0:
					elem = ex.getPdbId() + "_" + ex.getChain()
					if elem not in noCore:
						noCore.append(elem)

		self.l.log("NoCore: " + str(noCore))

		return len(noCore)

	def distributionCharts(self):
		otCore = []
		otNCore = []
		solvAccCore = []
		solvAccNCore = []

		#OY dla wszystkich wykresow
		hydroCore = []
		hydroNCore = []

		# hydrofobowowsc tylko dla Ca - zrobic parametryzacje albo skopiowac dla G

		for ex in self.exons:
			if ex.getHydroValCA()!='NA' and ex.getAvgSolventRelAcc()!='NA':

				if ex.isCoreCa() == True:
					otCore.append(float(ex.getOtCA()))
					hydroCore.append(float(ex.getHydroValCA()))
					solvAccCore.append(float(ex.getAvgSolventRelAcc()))


					if float(ex.getHydroValCA())<2.0:
						self.l.log("ODSTAJE1: " + ex.getExonId() + " : " + str(ex.getOtCA()))

					if float(ex.getAvgSolventRelAcc())>0.55:
						self.l.log("ODSTAJE2: " + ex.getExonId() + " : " + str(ex.getAvgSolventRelAcc()))
				else:
					otNCore.append(float(ex.getOtCA()))
					hydroNCore.append(float(ex.getHydroValCA()))
					solvAccNCore.append(float(ex.getAvgSolventRelAcc()))

			else:
				self.l.log("hydrofobowosc NA for exonId: " + ex.getExonId())

		#O/T to hydro

		#zapisa danych wykresu do pliku
		dataCore = zip(otCore, hydroCore, ['1']*len(otCore))
		dataNcore = zip(otNCore, hydroNCore, ['0']*len(otNCore))
		data = dataCore + dataNcore
		self.writeChartDataToFile('ot_to_hydro_distribution.csv', data)

		plt.plot(otCore, hydroCore, 'go', otNCore, hydroNCore, 'ro')
		plt.title('O/T to hydro distribution')
		plt.ylabel('hydrophobicity')
		plt.xlabel('O/T')
		plt.show()

		#solvent accesible surface to hydro

		#zapisa danych wykresu do pliku
		dataCore = zip(solvAccCore, hydroCore, ['1']*len(solvAccCore))
		dataNcore = zip(solvAccNCore, hydroNCore, ['0']*len(solvAccNCore))
		data = dataCore + dataNcore
		self.writeChartDataToFile('solvent_acc_to_hydro_distribution.csv', data)

		plt.plot(solvAccCore, hydroCore, 'go', solvAccNCore, hydroNCore, 'ro')
		plt.title('avg. solvent Acc. to hydro distribution')
		plt.ylabel('hydrophobicity')
		plt.xlabel('avg. solvent Acc')
		plt.show()


	def writeChartDataToFile(self, fileName, data):
		with open(fileName, 'wb') as f:
			writer = csv.writer(f)
			writer.writerows(data)

	def calculateExonsComposition(self):

		noCoreAminoAcidComposition = self.initialAminoAcidMap();
		coreAminoAcidComposition = self.initialAminoAcidMap();
		nCoreSecStrComposition = {};
		coreSecStrComposition = {};
		coreDomains = {}
		nCoreDomains = {}

		for ex in self.exons:
				aaSeq = ex.getAaSec();
				secStr = ex.getSecStrComposition()

				if ex.isCoreCa() == True:
					for aaCode in noCoreAminoAcidComposition.keys():
						coreAminoAcidComposition[aaCode]=coreAminoAcidComposition[aaCode]+aaSeq.count(aaCode)

					for secStrCode in secStr.keys():
						if secStrCode in coreSecStrComposition:
							coreSecStrComposition[secStrCode]=coreSecStrComposition[secStrCode]+secStr[secStrCode]
						else:
							coreSecStrComposition[secStrCode]=secStr[secStrCode]

					for d in ex.getDomains():
						if d.getPfamName() in coreDomains:
							coreDomains[d.getPfamName()]=coreDomains[d.getPfamName()]+1
						else:
							coreDomains[d.getPfamName()]=1

				else:
					for aaCode in noCoreAminoAcidComposition.keys():
						noCoreAminoAcidComposition[aaCode]=noCoreAminoAcidComposition[aaCode]+aaSeq.count(aaCode)

					for secStrCode in secStr.keys():
						if secStrCode in nCoreSecStrComposition:
							nCoreSecStrComposition[secStrCode]=nCoreSecStrComposition[secStrCode]+secStr[secStrCode]
						else:
							nCoreSecStrComposition[secStrCode]=secStr[secStrCode]

					for d in ex.getDomains():
						if d.getPfamName() in nCoreDomains:
							nCoreDomains[d.getPfamName()]=nCoreDomains[d.getPfamName()]+1
						else:
							nCoreDomains[d.getPfamName()]=1


		noCoreAminoAcidCompositionCnt = 0.0
		coreAminoAcidCompositionCnt = 0.0
		nCoreSecStrCompositionCnt = 0.0
		coreSecStrCompositionCnt = 0.0

		for i in noCoreAminoAcidComposition.values():
			noCoreAminoAcidCompositionCnt=noCoreAminoAcidCompositionCnt+i
		for i in coreAminoAcidComposition.values():
			coreAminoAcidCompositionCnt=coreAminoAcidCompositionCnt+i
		for i in nCoreSecStrComposition.values():
			nCoreSecStrCompositionCnt=nCoreSecStrCompositionCnt+i
		for i in coreSecStrComposition.values():
			coreSecStrCompositionCnt=coreSecStrCompositionCnt+i

		if noCoreAminoAcidCompositionCnt!= 0:
			for k in noCoreAminoAcidComposition.keys():
				noCoreAminoAcidComposition[k]=self.format2(noCoreAminoAcidComposition[k]/noCoreAminoAcidCompositionCnt*100)
		if coreAminoAcidCompositionCnt!= 0:
			for k in coreAminoAcidComposition.keys():
				coreAminoAcidComposition[k]=self.format2(coreAminoAcidComposition[k]/coreAminoAcidCompositionCnt*100)
		if nCoreSecStrCompositionCnt!= 0:
			for k in nCoreSecStrComposition.keys():
				nCoreSecStrComposition[k]=self.format2(nCoreSecStrComposition[k]/nCoreSecStrCompositionCnt*100)
		if coreSecStrCompositionCnt!= 0:
			for k in coreSecStrComposition.keys():
				coreSecStrComposition[k]=self.format2(coreSecStrComposition[k]/coreSecStrCompositionCnt*100)

		self.l.log("noCoreAminoAcidComposition: " + str(noCoreAminoAcidComposition))
		self.l.log("coreAminoAcidComposition: " + str(coreAminoAcidComposition))
		self.l.log("nCoreSecStrComposition: " + str(nCoreSecStrComposition))
		self.l.log("coreSecStrComposition: " + str(coreSecStrComposition))
		self.l.log("coreDomains: " + str(coreDomains))
		self.l.log("nCoreDomains: " + str(nCoreDomains))

		resultFile = open("exonComposition.txt", "w")
		resultFile.write("noCoreAminoAcidComposition:\n")
		for key, value in noCoreAminoAcidComposition.iteritems():
			resultFile.write(str(key) + " - " + str(value))
			resultFile.write("\n")
		resultFile.write("\ncoreAminoAcidComposition:\n")
		for key, value in coreAminoAcidComposition.iteritems():
			resultFile.write(str(key) + " - " + str(value))
			resultFile.write("\n")
		resultFile.write("\nnCoreSecStrComposition:\n")
		for key, value in nCoreSecStrComposition.iteritems():
			resultFile.write(str(key) + " - " + str(value))
			resultFile.write("\n")
		resultFile.write("\ncoreSecStrComposition:\n")
		for key, value in coreSecStrComposition.iteritems():
			resultFile.write(str(key) + " - " + str(value))
			resultFile.write("\n")
		resultFile.write("\ncoreDomains:\n")
		for key, value in coreDomains.iteritems():
			resultFile.write(str(key) + " - " + str(value))
			resultFile.write("\n")
		resultFile.write("\nnCoreDomains:\n")
		for key, value in nCoreDomains.iteritems():
			resultFile.write(str(key) + " - " + str(value))
			resultFile.write("\n")
		resultFile.close()



	def initialAminoAcidMap(self):
		map={}
		map['A'] = 0
		map['R'] = 0
		map['N'] = 0
		map['D'] = 0
		map['C'] = 0
		map['E'] = 0
		map['Q'] = 0
		map['G'] = 0
		map['H'] = 0
		map['I'] = 0
		map['L'] = 0
		map['K'] = 0
		map['M'] = 0
		map['F'] = 0
		map['P'] = 0
		map['S'] = 0
		map['T'] = 0
		map['W'] = 0
		map['Y'] = 0
		map['V'] = 0
		return map
