from customLogger import Logger

class OntologyInfo:

	l = Logger()
	
	def __init__(self, ID, name, definition, synonyms, ontology, pdb, chain):
		self.ID = ID
		self.name = name
		self.definition = definition
		self.synonyms = synonyms
		self.ontology = ontology
		self.pdb = pdb
		self.chain = chain
		
	# id
	def getID(self):
		return self.ID
		
	# name
	def getName(self):
		return self.name
		
	# definition
	def getDefinition(self):
		return self.definition
		
	# synonyms
	def getSynonyms(self):
		return self.synonyms
	
	#ontology: B - Biological Process 
	#ontology: C - Cellular Component 
	#ontology: F - Molecular Function
	def getOntology(self):
		return self.ontology
		
	def getPdb(self):
		return self.pdb

	def getChain(self):
		return self.chain		
		
	def printInfo(self):
		self.l.logF('ONTOLOGY ID: ' + self.ID + ', pdb: '  + self.pdb + ' chain: ' + self.chain)
		self.l.logF('name: ' + self.name)
		self.l.logF('definition: ' + self.definition)
		self.l.logF('synonyms: ' + self.synonyms)
		self.l.logF('ontology: ' + self.ontology)
		
	def getFullData(self):
		return (self.ID, self.pdb, self.chain, self.ontology, self.name, self.definition, self.synonyms)