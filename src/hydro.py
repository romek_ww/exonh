
# CHANGELOG:
# - pobrawienie wczytywania atomow z PDB parsera i bledu wywalajacego program jesli wystepowal atom CA w seksji HETATM
# - pobrawienie zapisyewania plikow wyjsciowych dla kilku lancuchow
# - dodanie kolumn z hydrofobowoscia liczona dla centum geometrycznego residua (po wszystkich atomach)
#   do juz istniejacych kolumn z wartoscia wyliczona dla atomu C-alpha
# - zmiana progu 'atleast' w self.computeMatrix z 1 na 0.99
# - usuniecie niepotrzebnego kodu
# - poprawa pomijania 'siebie' przy liczeniuy hydrofobowowsci oczekiwanej
# - poprawa funkcji wielomianowej przy hydrofobowowsci oczekiwanej
# - okomentowanie

from Bio.PDB.PDBParser import PDBParser
import numpy
import math
from itertools import groupby
import sys
import os
import datetime
from customLogger import Logger

class Hydro:

	l = Logger()

	def __init__(self, file, table, cutoff, dot, notgauss):
		if file == None or table == None:
			self.l.error("ERROR: no filename or tablefile")
			sys.exit()
		self._filename = file
		self._tablefile = table
		self._c = cutoff
		self._dotProductThreshold = dot
		self._oneminiusgauss = notgauss
		

	def computeWeightCenter(self, atoms):
		n = len(atoms)*1.
		#print sum(atoms)
		(x,y,z) = sum(atoms)
		#print (x,y,z)
		return (x/n,y/n,z/n) 

	def computeMatrix(self, A,B):
		nA = A/numpy.linalg.norm(A)
		nB = B/numpy.linalg.norm(B)
		nB = nB - numpy.dot(nB,nA)*nA # those two lines improve, the orthogonality
		nB = nB/numpy.linalg.norm(nB) # I'm going to imporove B so that it is really orthogonal, then the matrix can be an isometry
		atleast = 0.99
		notZero = 0.1
		for i in range(3): # it suffices to check some three orthognal to each other vectors
			l = [0,0,0]
			l[i] = 1
			e = numpy.array(l)
			#print str(numpy.dot(nA,e)) + " " + str(numpy.dot(nB,e))
			if abs(numpy.dot(nA,e)) + abs(numpy.dot(nB,e)) > atleast and abs(numpy.dot(nA,e)) < 1.0 and abs(numpy.dot(nB,e)) < 1.0:
				v = e - numpy.dot(nA,e)*nA - numpy.dot(nB,e)*nB # u could check that this gives us an orthoonal vector, orthogonality gramma-Schmmita 
				nV = v/(numpy.linalg.norm(v)) # normalise is it
				# test
				u = [nA,nB,nV] # list of vectors
				tmpMatrix = numpy.array(u)
				return tmpMatrix


	# odchylenie standardowe, nie wyciagam pierwiasta bo potem jest do kwadratu
	def var(self, atoms,i,center):
		n = len(atoms)
		#deb = map(lambda x : x[i],atoms)
		#print 'deb: ' + str(deb)
		return sum(map(lambda x : (x[i] - center)**2,atoms))/(n-1)
		
	def evalFunction(self, variance,centers,atom):
		res = 1.0
		for i in range(0,3):
			res = res * math.exp(-((atom[i] - centers[i])**2)/(2*variance[i]))
		return res

	def moveToCenter(self, atoms):
		center = self.computeWeightCenter(atoms)
		return map(lambda x : x - center,atoms)

	def computeX(self, atoms):
		tmpLengths = [numpy.linalg.norm(x) for x in atoms]
		Aindex = tmpLengths.index(max(tmpLengths))
		A = atoms[Aindex]
		return A


	def computeY(self, atoms,A):
		normA = numpy.linalg.norm(A)
		#TODO: sprawdziz ten wzor nizej:
		ortognalToA = filter(lambda x : abs(numpy.dot(x,A)) < self._dotProductThreshold * normA * numpy.linalg.norm(x),atoms) # tak naprawde sprawdzamy czy cosinus kata pomiedzy jest mniejszy od threshold
		ortTmpLengths = map(numpy.linalg.norm,ortognalToA)
		if not ortTmpLengths:
			raise RuntimeError('no ortogonal, cannot calculate hydro')
		B = ortognalToA[ortTmpLengths.index(max(ortTmpLengths))] 
		return B 

	def transformPoins(self, atoms,mMatrix):
		return map(lambda x: numpy.dot(mMatrix,x),atoms)

	def transofrm(self, atoms):
		# pzesowa wspolrzedne tak ze srodek geometryczny wszystkich wspolrzednych
		# znajduje sie w (0,0,0)
		atoms = self.moveToCenter(atoms)
		#zwraca wspolrzedne atomu najbardziej oddalonego od srodka
		A = self.computeX(atoms)
		#najdluzszy ze zboru 'prostopadlych' do A
		B = self.computeY(atoms,A)
		#zwraca wektory bazy w nowym ukladzie wspolrzednych
		mMatrix = self.computeMatrix(A,B)
		if mMatrix == None:
			raise RuntimeError('blad wyznaczania hydrofobowosci teoretycznej - nie mozna obliczyc wektorow bazy w nowym ukladzie wspolrzednych')
		#transformuje punkty do nowego ukladu wspolrzednych
		moved = self.transformPoins(atoms,mMatrix)
		return moved

	def evalFirstFunction(self, moved):
		center = self.computeWeightCenter(moved) # should be [0,0,0]
		variance = [0,0,0]
		for i in range(0,3):
			variance[i] = self.var(moved,i,center[i])
		Ht = map(lambda x : self.evalFunction(variance,center,x), moved)
		if self._oneminiusgauss:
			Ht = map(lambda x : 1-x,Ht)
		Htsum = sum(Ht)
		#Htnorm = map(lambda x : x/Htsum, Ht) #return (map(lambda x: x/s, observedRaw), s)
		Htnorm =  Ht #return (map(lambda x: x/s, observedRaw), s)
		return Htnorm


	def polynomailFunction(self, x):
		return 1.0 - 0.5*(7*(x**2) -9*(x**4) + 5*(x**6) - (x**8))


	def evalObserved(self, names,coords,dict):
		n = len (coords)
		observedRaw = []
		for i in range(0,n):
			res = 0
			for j in range(0,n):
				# siebie tez bierze pod uwage
				#if i == j:
				#	continue
				rij = numpy.linalg.norm(coords[i] - coords[j])
				if rij <= self._c:
					if names[j] not in dict:
						raise RuntimeError('brak aminokwasu w tabeli hydrofobowosci: ' + names[j])
					if names[i] not in dict:
						raise RuntimeError('brak aminokwasu w tabeli hydrofobowosci: ' + names[i])	
					res = res + (dict[names[i]] + dict[names[j]])*self.polynomailFunction(rij/self._c)
			observedRaw.append(res)

		s = sum(observedRaw)
		#return (map(lambda x: x/s, observedRaw), s)
		return (observedRaw, s)

	def getChains(self, x,file):
		parser = PDBParser()
		structure = parser.get_structure(x,file)

		return structure.get_chains()
		
	def removeHeteroAtoms(self, chain):
		return filter(lambda x: x.get_id()[0] == ' ', chain.get_residues())

	def getCoordinatesCa(self, residues):
		caAtomCords = []
		for res in residues:
			atoms = res.get_unpacked_list()
			caAtom = filter(lambda x : x.get_fullname().strip() == 'CA',atoms) # filter C-alpha atoms
			if len(caAtom)==0:
				#blad w pdb, brak atomu CA ?? zastepujemy geometrycznym
				caAtomCords.append(self.getCoordinatesGForRes(atoms))
			else:
				caAtomCords.append(caAtom[0].get_coord())
			
		return caAtomCords

	def getCoordinatesG(self, residues):
		gResCords = []
		for res in residues:
			atoms = res.get_unpacked_list()
			crd = self.getCoordinatesGForRes(atoms)
			gResCords.append(crd)
			
		return gResCords
		
	def getCoordinatesGForRes(self, atoms):
		atomCords = [0, 0, 0]
		count = 0
		for atom in atoms:
			atomCords = atomCords + atom.get_coord()
			count=count+1
			
		return (atomCords/count)	
		
	def getResiudeName(self, residues):
		return [res.get_resname() for res in residues] 

	def getPdbNos(self, residues):
		return [res.get_id()[1] for res in residues]
		
	def parseTable(self, tableFile):
		table = {}
		f = open(tableFile,'r')
		for line in f:
			if len(line) < 7:
				continue
			z  = line.rstrip().split(' ')
			table[z[0]] = float(z[1])
		return table

	def getHeader(self):
		h1 = "HTHEOR_CA_" + (self._oneminiusgauss and "1-Gauss" or "Gauss")
		h2 = "HOBSER_CA"
		h3 = "HTHEOR_G_" + (self._oneminiusgauss and "1-Gauss" or "Gauss")
		h4 = "HOBSER_G"
		return [('RES', 'PDB_NO', h1, h2, h3, h4)]
		
	def writeCsv(self, outputfile,result):
		import csv
		result = self.getHeader() + result
		with open(outputfile, 'wb') as f:
			writer = csv.writer(f)
			writer.writerows(result)


	def computeOutputFileName(self, chain,ext):
		realFileName = os.path.basename(self._filename)
		realFileNoExt = realFileName.split('.')[0]

		realTableName = os.path.basename(self._tablefile)
		realTableNoExt = realTableName.split('.')[0]

		#return realFileNoExt + "_" + str(chain) +"." + ext
		return "_".join([realFileNoExt, realTableNoExt,(self._oneminiusgauss and "1-Gauss" or "Gauss")]) + "_" + chain + "." + ext

	def folderName(self):
		
		realFileName = os.path.basename(self._filename)
		realFileNoExt = realFileName.split('.')[0]
		
		realTableName = os.path.basename(self._tablefile)
		realTableNoExt = realTableName.split('.')[0]

		return 'hydro/' + "_".join([str(realFileNoExt),str(realTableNoExt), (self._oneminiusgauss and "1-Gauss" or "Gauss")])


	def start(self, chainIdn):
		chains = self.getChains(self._filename,self._filename)

		table = self.parseTable(str(self._tablefile))
		folder = self.folderName()
		if not os.path.exists(folder):
			os.makedirs(folder)
		
		chain = 'NA'
		chainRes = []
		sum_raw_ca=0
		sum_raw_g=0
			
		for ch in chains:
			if chainIdn==ch.get_id():
				chain=ch

		try:
		
			if chain != 'NA':	
				filteredAtoms = self.removeHeteroAtoms(chain)
				names = self.getResiudeName(filteredAtoms)
				pdbNos = self.getPdbNos(filteredAtoms)
			
				coords_ca = self.getCoordinatesCa(filteredAtoms)
				movedy_ca = self.transofrm(coords_ca)
				theo_ca = self.evalFirstFunction(movedy_ca)
				obser_ca, sum_raw_ca = self.evalObserved(names,coords_ca,table)
				
				coords_g = self.getCoordinatesG(filteredAtoms)
				movedy_g = self.transofrm(coords_g)
				theo_g = self.evalFirstFunction(movedy_g)
				obser_g, sum_raw_g = self.evalObserved(names,coords_g,table)
				
				# ot_ca = self.calculateOT(obser_ca, theo_ca)
				# or_ca = self.calculateOR(obser_ca)
				# ot_g = self.calculateOT(obser_g, theo_g)
				# or_g = self.calculateOR(obser_g)
				
				result = zip(names, pdbNos, theo_ca, obser_ca, theo_g, obser_g)
				
				resultCSV = os.path.join(folder,self.computeOutputFileName(str(chain.get_id()),'csv'))
				self.writeCsv(resultCSV, result)
				
				chainRes = zip(names, pdbNos, obser_ca, obser_g, theo_ca, theo_g)
			else:
				self.l.error('ERROR chain ' + chainIdn + ' from ' + self._filename + 'not found')
		except RuntimeError as msg:
			self.l.error('ERROR processin chain ' + chainIdn + ' from ' + self._filename + ', message: ' + str(msg))
			
		# dla kazdego lancucha: lista krotek
		return (chainRes, folder, sum_raw_ca, sum_raw_g)


	# def calculateOT(self, o, t):
	# 	ret = []
	#
	# 	for x, y in zip(o, t):
	# 		if x!=0:
	# 			#ret.append(x*math.log(x, 2)/y)  - bez nawaiasow, jak w pracy
	# 			ret.append(x*math.log(x/y, 2))
	# 		else:
	# 			ret.append(0.0);
	#
	# 	return ret
    #
	# def calculateOR(self, o):
	# 	r = 1.0/len(o)
	# 	ret = []
	#
	# 	for x in o:
	# 		if x!=0:
	# 			#ret.append(x*math.log(x, 2)/r)
	# 			ret.append(x*math.log(x/r, 2))
	# 		else:
	# 			ret.append(0.0);
	#
	# 	return ret
	
