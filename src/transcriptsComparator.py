from Bio.PDB.PDBParser import PDBParser
import requests, sys
from Bio.Blast.Applications import NcbiblastpCommandline
from StringIO import StringIO
from Bio.Blast import NCBIXML
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO
from Bio import pairwise2
from Bio.SubsMat import MatrixInfo as matlist
import os.path
from customLogger import Logger


class TranscriptsComparator:
	
	l = Logger()
	
	aaCodes = dict()
	aaCodes['GLY'] = 'G' #GLYCINE
	aaCodes['PRO'] = 'P' #PROLINE
	aaCodes['ALA'] = 'A' #ALANINE
	aaCodes['VAL'] = 'V' #VALINE
	aaCodes['LEU'] = 'L' #LEUCINE
	aaCodes['ILE'] = 'I' #ISOLEUCINE
	aaCodes['MET'] = 'M' #METHIONINE
	aaCodes['CYS'] = 'C' #CYSTEINE
	aaCodes['PHE'] = 'F' #PHENYLALANINE
	aaCodes['TYR'] = 'Y' #TYROSINE
	aaCodes['TRP'] = 'W' #TRYPTOPHAN
	aaCodes['HIS'] = 'H' #HISTIDINE
	aaCodes['LYS'] = 'K' #LYSINE
	aaCodes['ARG'] = 'R' #ARGININE
	aaCodes['GLN'] = 'Q' #GLUTAMINE
	aaCodes['ASN'] = 'N' #ASPARAGINE
	aaCodes['GLU'] = 'E' #GLUTAMIC ACID
	aaCodes['ASP'] = 'D' #ASPARTIC ACID
	aaCodes['SER'] = 'S' #SERINE
	aaCodes['THR'] = 'T' #THREONINE 

	def findBest(self, pdb, transcriptStr, chainNo):
	
		self.l.logF('find best for chainNo: ' + str(chainNo))
	
		transcripts = transcriptStr.split(',')

		pdbFile='./pdb/' + pdb + '.pdb'

		#pobranie sekwencji pdb

		parser = PDBParser()
		structure = parser.get_structure(pdbFile,pdbFile)

		chains = structure.get_chains()

		chainId = "NA"
		
		seqPdb=''
		
		for ch in chains:
			if ch.get_id()==chainNo:
				self.l.logF('znaleziono')
				chainId = ch.get_id()
				residuesObj = ch.get_unpacked_list()
				chainSeq =''
				for res in residuesObj:
					if res.get_resname() in self.aaCodes.keys():
						chainSeq = chainSeq + self.aaCodes[res.get_resname()]

				#zapisanie pdb seq do pliku

				self.l.logF('zapisywanie sekwencji pdb(' + str(len(chainSeq)) + ') dla: ' + pdb + '_' + chainId)
				
				seqPdb = Seq(chainSeq, IUPAC.protein)
				sRecord = SeqRecord(seqPdb, id=pdb)
				
				pdbFasta='./pdb/' + pdb + '_' + chainId + '.fasta'
				output_handle = open(pdbFasta, "w")
				SeqIO.write(sRecord, output_handle, "fasta")
				break;
		
		if seqPdb=='':
			#przy konwersji id pobralismy lancuch ktorego nie ma w pliku pdb (np dwa uniprot entry name a tylko jeden lancuch w pdb)
			raise RuntimeError('chain not found in PDB file, chain no:' + str(chainNo))
		
		#pobranie sekwencji transkrytow z ensembl

		server = "http://rest.ensembl.org"

		matrix = matlist.blosum62
		gap_open = -5
		gap_extend = -0.1

		bestId=''
		assign='NA'
		bestScore = 'init'

		for transcriptId in transcripts:
			#sprawdzamy czy plik nie jest juz na dysku
			file = './transcripts/' + transcriptId + '.fasta'
			
			if not os.path.isfile(file):
				self.l.logF('pobieranie sekwencji dla ' + transcriptId)
				
				#dla transkryptu pobieramy sekwencje CDS - tylko czesc kodujaca bialko
				#bez regionow UTR
				ext = '/sequence/id/' + transcriptId + '?type=cds'
			 
				r = requests.get(server+ext, headers={ "Content-Type" : "text/x-fasta"})
			 
				if not r.ok:
					self.l.error("ERROR dowloading sequence for: " + ext)
					continue
			 
				trsOut = r.text
				
				trsSeq = ''
				first = True
				for line in trsOut.split('\n'):
					if first==False and len(line)>0:
						trsSeq=trsSeq+line
					else:
						first=False
						
				
				# zapis na dysk sekwencji NN
				
				fileNN = './transcripts/' + transcriptId + '_NN.fasta'
				
				#seq = Seq(trsSeq, IUPAC.unambiguous_dna)
				seq = Seq(trsSeq, IUPAC.ambiguous_dna)
				
				sRecordNN = SeqRecord(seq, id=transcriptId)
				output_handleNN = open(fileNN, "w")
				SeqIO.write(sRecordNN, output_handleNN, "fasta")
				
				seq = seq.translate()
				sRecord = SeqRecord(seq, id=transcriptId)
				
				output_handle = open(file, "w")
				SeqIO.write(sRecord, output_handle, "fasta")
			else:
				self.l.logF('znaleziona na dysku: ' + transcriptId)
				file = SeqIO.parse(open(file), "fasta")
				record = next(file)
				seq = str(record.seq)
				
			# porownanie sekwencji pdb i danego transkryptu

			
			seqPdbA = str(seqPdb)
			seqTrsA = str(seq).replace('*', '')
			 
			if seqPdbA=='':
				self.l.log('brak sekwencji pdb dla  ' + pdb + '_' + chainId)
				continue
			
			if seqTrsA=='':
				self.l.log('brak sekwencji transkryptu dla  ' + transcriptId)
				continue
			
			#http://biopython.org/DIST/docs/api/Bio.pairwise2-module.html 
			# x - Identical characters have score of 1, otherwise 0
			# s - Same open and extend gap penalties for both sequences
			
			self.l.logF('porownoje:')
			self.l.logF(seqPdbA)
			self.l.logF(seqTrsA)
			
			alns = pairwise2.align.globalxs(seqPdbA, seqTrsA, gap_open, gap_extend)
			 
			top_aln = alns[0]
			alnPdb, alnTrs, score, begin, end = top_aln
			
			self.l.logF('***ALIGN FOR: ' + transcriptId + '***')
			self.l.logF(alnPdb)
			self.l.logF(alnTrs)
			self.l.logF('score: ' + str(score) + ' begin: ' + str(begin) + ' end: ' + str(end))
			
			if str(bestScore)=='init' or bestScore<score:
				bestScore=score
				bestId = transcriptId
				assign = [alnPdb, alnTrs]
		
		return 	[bestId, assign]