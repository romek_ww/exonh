import requests
from xml.dom import minidom
from ontologyInfo import OntologyInfo
from pfamDomain import PfamDomain
from customLogger import Logger
import os
import pickle

class PdbInfo:

	l = Logger()

	def getInfo(self, pdbId):
		
		ontologyCacheFile = './cache/' + pdbId + '/ontology.obj'
		pfamCacheFile = './cache/' + pdbId + '/pfam.obj'
	
		if os.path.exists('./cache/' + pdbId):
			self.l.log("getting pfam domains/ontologies from cache")
			
			with open(ontologyCacheFile,'rb') as f:
				ontologies = pickle.load(f)
			
			with open(pfamCacheFile,'rb') as f:
				pfamDomains = pickle.load(f)
				
		else:
			self.l.log("not found in cache, getting from RCSB")
			
			onthUrl = 'http://www.rcsb.org/pdb/rest/goTerms?structureId=' + pdbId
			pfamUrl = 'http://www.rcsb.org/pdb/rest/hmmer?structureId=' + pdbId
			
			ontologies = []
			pfamDomains = []
			
			#Gene Ontology terms for PDB chains
			
			r = requests.get(onthUrl)

			if not r.ok:
				r.raise_for_status()
				sys.exit()
						 
			onthXml = r.text

			onthXmldoc = minidom.parseString(onthXml)
			
			terms = onthXmldoc.getElementsByTagName('term')
			
			for term in terms:
				goId = term.attributes['id'].value
				chain = term.attributes['chainId'].value
				detail = term.getElementsByTagName('detail')[0]
				name = detail.attributes['name'].value if detail.hasAttribute('name') else ''
				definition = detail.attributes['definition'].value if detail.hasAttribute('definition') else ''
				synonyms = detail.attributes['synonyms'].value if detail.hasAttribute('synonyms') else ''
				ontology = detail.attributes['ontology'].value if detail.hasAttribute('ontology') else ''
				
				ontObj = OntologyInfo(goId, name, definition, synonyms, ontology, pdbId, chain)
				ontologies.append(ontObj)
				
			
				#ontology: B - Biological Process 
				#ontology: C - Cellular Component 
				#ontology: F - Molecular Function		
				
		
			#Pfam annotations for PDB entries

			r = requests.get(pfamUrl)

			if not r.ok:
				r.raise_for_status()
				sys.exit()
						 
			pfamXml = r.text
			
			pfamXmldoc = minidom.parseString(pfamXml)
			
			pfamHits = pfamXmldoc.getElementsByTagName('pfamHit')
			
			for hit in pfamHits:
				pdbResNumStart = hit.attributes['pdbResNumStart'].value if hit.hasAttribute('pdbResNumStart') else ''
				pdbResNumEnd = hit.attributes['pdbResNumEnd'].value if hit.hasAttribute('pdbResNumEnd') else ''
				pfamAcc = hit.attributes['pfamAcc'].value if hit.hasAttribute('pfamAcc') else ''
				pfamName = hit.attributes['pfamName'].value if hit.hasAttribute('pfamName') else ''
				pfamDesc = hit.attributes['pfamDesc'].value if hit.hasAttribute('pfamDesc') else ''
				eValue = hit.attributes['eValue'].value if hit.hasAttribute('eValue') else ''
				chain = hit.attributes['chainId'].value if hit.hasAttribute('chainId') else ''
				
				pfamObj = PfamDomain(pdbResNumStart, pdbResNumEnd, pfamAcc, pfamName, pfamDesc, eValue, pdbId, chain)
				pfamDomains.append(pfamObj)
			
			#zapisanie do cache			
			
			os.makedirs('./cache/' + pdbId)
			
			with open(ontologyCacheFile, 'wb') as f:
				pickle.dump(ontologies, f)
	
			with open(pfamCacheFile, 'wb') as f:
				pickle.dump(pfamDomains, f)
				
		return [ontologies, pfamDomains]