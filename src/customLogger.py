from termcolor import cprint
import colorama

colorama.init()

class Logger:
	
	RED='red'
	GREEEN='green'
	YELLOW='yellow'
	BLUE='blue'
	WHITE='white'
	
	f = open('main.log', 'w')
	ferr = open('err.log', 'w')
	
	# wypisywanie na ekran i zapis do logu
	def log(self, s, color=WHITE):
		cprint(s, color)
		self.logF(s)
		
	# zapis do logu	
	def logF(self, s):
		self.f.write(str(s)+'\n')
		
	#zalogowanie bledu
	def error(self, s):
		self.log(s, self.RED)
		self.ferr.write(str(s)+'\n')