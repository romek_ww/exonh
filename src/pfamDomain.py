from customLogger import Logger

class PfamDomain:

	l = Logger()

	def __init__(self, pdbResNumStart, pdbResNumEnd, pfamAcc, pfamName, pfamDesc, eValue, pdb, chain):
		self.pdbResNumStart = pdbResNumStart
		self.pdbResNumEnd = pdbResNumEnd
		self.pfamAcc = pfamAcc
		self.pfamName = pfamName
		self.pfamDesc = pfamDesc
		self.eValue = eValue
		self.pdb = pdb
		self.chain = chain
		
	# poczatek domeny w PDB
	def getPdbResNumStart(self):
		return self.pdbResNumStart
		
	# koniec domeny w PDB
	def getPdbResNumEnd(self):
		return self.pdbResNumEnd
		
	# pfamAcc
	def getPfamAcc(self):
		return self.pfamAcc
		
	# pfamName
	def getPfamName(self):
		return self.pfamName
	
	# pfamDesc
	def getPfamDesc(self):
		return self.pfamDesc
	
	# uniprotId bialka (jesli bialko ma kilka lancochow beda one mialy to samo pdbId i
	# rozne uniprotId)
	def getEValue(self):
		return self.eValue
		
	def getPdb(self):
		return self.pdb

	def getChain(self):
		return self.chain	
		
	def printInfo(self):
		self.l.logF('pfamDomain for pdb: ' + self.pdb + ' chain: ' + self.chain)
		self.l.logF('pdbResNumStart: ' + self.pdbResNumStart)
		self.l.logF('pdbResNumEnd: ' + self.pdbResNumEnd)
		self.l.logF('pfamAcc: ' + self.pfamAcc)
		self.l.logF('pfamName: ' + self.pfamName)
		self.l.logF('pfamDesc: ' + self.pfamDesc)
		self.l.logF('eValue: ' + self.eValue)
		
	def getFullData(self):
		return (self.pfamAcc, self.pdb, self.chain, self.pfamName, self.pdbResNumStart, self.pdbResNumEnd, self.eValue, self.pfamDesc)	