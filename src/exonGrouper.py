from os import listdir
from os.path import isfile, join
from customLogger import Logger
import math

class ExonGrouper:

	l = Logger()

	def run(self, data, folderName, pdbName, chainIdn):
		chainData=data
		
		sums = self.calculateSums(chainData)
		
		coreChecked =self.isCore(sums)
		
		fileName = pdbName + '_' + chainIdn + '_sums.csv'
		self.writeToFile(coreChecked, folderName, fileName)

		return coreChecked


	def calculateSums(self, data):
		sums = {}
		out = {}

		exonsLengths = {}
		obserSums = {}
		theoSums = {}
		obserGSums = {}
		theoGSums = {}

		for aminoAcid in data:
			exonNo = aminoAcid[6]
			if exonNo in exonsLengths:
				exonsLengths[exonNo]=exonsLengths[exonNo]+1
				obserSums[exonNo]=obserSums[exonNo]+aminoAcid[2]
				theoSums[exonNo]=theoSums[exonNo]+aminoAcid[4]
				obserGSums[exonNo]=obserGSums[exonNo]+aminoAcid[3]
				theoGSums[exonNo]=theoGSums[exonNo]+aminoAcid[5]
			else:
				exonsLengths[exonNo]=1
				obserSums[exonNo]=aminoAcid[2]
				theoSums[exonNo]=aminoAcid[4]
				obserGSums[exonNo]=aminoAcid[3]
				theoGSums[exonNo]=aminoAcid[5]

		for aminoAcid in data:
			exonNo = aminoAcid[6]
			if exonNo in sums:
				tmp = sums[exonNo]
			else:
				tmp = (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)

			theoCaNorm=aminoAcid[4]/theoSums[exonNo]
			obserCaNorm=aminoAcid[2]/obserSums[exonNo]
			ot_ca=self.calculateOT(obserCaNorm, theoCaNorm)
			or_ca=self.calculateOR(obserCaNorm, exonsLengths[exonNo])

			theoGNorm=aminoAcid[5]/theoGSums[exonNo]
			obserGNorm=aminoAcid[3]/obserGSums[exonNo]
			ot_g=self.calculateOT(obserGNorm, theoGNorm)
			or_g=self.calculateOR(obserGNorm, exonsLengths[exonNo])

			currentTuple = 	(ot_ca, or_ca, ot_g, or_g, aminoAcid[2], aminoAcid[3], aminoAcid[4], aminoAcid[5])
			tmp = tuple(sum(x) for x in zip(tmp, currentTuple))
			sums[exonNo] = tmp

		for exonNo, exonData in sums.iteritems():
			exonLength = exonsLengths[exonNo]
			out[exonNo] = exonData[0:4] + (exonData[4]/exonLength, exonData[5]/exonLength, exonData[6]/exonLength, exonData[7]/exonLength)

		return out
		
	def isCore(self, data):
		out = {}
		for exonName in data.keys():
			exon = data[exonName]
			isCoreCa = False
			isCoreG = False
			
			if exon[0]<exon[1]:
				isCoreCa = True
				
			if exon[2]<exon[3]:
				isCoreG = True
			
			
			#wyliczenie wspolczynnika RD dla hydrofobowosci liczonej w CA i G
			rdCa = exon[0]/(exon[0]+exon[1])
			rdG = exon[2]/(exon[2]+exon[3])
			
			out[exonName] = exon + (isCoreCa, isCoreG, rdCa, rdG)
			
		return out
		
	def writeToFile(self, data, folderName, fileName):
		import csv
		header = [('EXON_ID', 'O/T_CA', 'O/R_CA', 'O/T_G', 'O/R_G', 'HYDRO_CA', 'HYDRO_G', 'THEO_CA', 'THEO_G', 'IS_CORE_CA', 'IS_CORE_G', 'RD_CA', 'RD_G')]
		ret = self.toList(data)
		ret = header + ret
		outputfile = join(folderName,fileName)
		with open(outputfile, 'wb') as f:
			writer = csv.writer(f)
			writer.writerows(ret)
			
			
	def toList(self, data):
		out = []
		for k in data.keys():
			tmp = (k, ) + data[k]
			out.append(tmp)
			
		return out

	def calculateOR(self, o, length):
		r = 1.0/length

		if o!=0:
			return o*math.log(o/r, 2)
		else:
			return 0.0

	def calculateOT(self, o, t):

		if o!=0:
			return o*math.log(o/t, 2)
		else:
			return 0.0
