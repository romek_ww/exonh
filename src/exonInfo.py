import requests
from xml.dom import minidom
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC

class ExonInfo:

	def getExons(self, transcriptId):

		url = 'http://ws.bioinfo.cipf.es/cellbase/rest/latest/hsa/feature/transcript/' + transcriptId + '/exon'
	
		r = requests.get(url)

		if not r.ok:
			r.raise_for_status()
			sys.exit()
					 
		trsOut = r.text

		exonOut=[]
		
		first = True
		for line in trsOut.split('\n'):
			if first==False and len(line)>0:
				sp = line.split('\t')
				exonOut.append([x.encode('utf-8') for x in sp])
			else:
				first=False
		
		
		#exonOut:
		#tablica z kolumnami dla kazdego exonu:
		#Ensembl ID, chromosome, start, end, strand
		
		exons = {}
		
		for exon in exonOut:
			exSeq = self.getSequence(exon[1], exon[2], exon[3])
			exons[exon[0]] = exSeq
			
		return exons
		
	def getSequence(self, chromosome, start, end):
		
		url = 'http://genome.ucsc.edu/cgi-bin/das/hg38/dna?segment=chr' + chromosome + ':' + start + ',' + end
		
		r = requests.get(url)		

		if not r.ok:
			r.raise_for_status()
			sys.exit()
			
		seqResp = r.text
		seqXml = minidom.parseString(seqResp)
		tmp = seqXml.getElementsByTagName('DNA')[0].childNodes[0].toxml()
		seq = ''.join(tmp.split())
		
		seqAA = Seq(seq, IUPAC.unambiguous_dna)
		seqAA = seqAA.translate()
		
		return str(seq)
		
		
		